#include "Core/ExpressionGenerator.h"
#include <type_traits>

template <class Generator>
ExpressionGenerator<Generator>::ExpressionGenerator()
{
}

template <class Generator>
template <class FunctionOp>
size_t ExpressionGenerator<Generator>::Insert(const FunctionOp& f,bool bdepth)
{
    std::type_index index(typeid(typename FunctionOp::TypeFunctionNonML::result_type));
    size_t func=FuncCreation(f);
    m_list.insert({index,func});
    if(bdepth)
    {
        m_listDepth.insert({index,func});
    }
    return func;
}

template <class Generator>
template <class FunctionOp,class FuncGenerate>
size_t ExpressionGenerator<Generator>::Insert(const FunctionOp& f,FuncGenerate funcGenerate,bool bdepth)
{
    std::type_index index(typeid(typename FunctionOp::TypeFunctionNonML::result_type));
    size_t func=FuncCreation(f,funcGenerate);
    m_list.insert({index,func});
    if(bdepth)
    {
        m_listDepth.insert({index,func});
    }
    return func;
}

template <class Generator>
template <class FunctionOp>
size_t ExpressionGenerator<Generator>::FuncCreation(const FunctionOp& f)
{
    size_t id=m_funcList.size();
    std::function<Expressions(Generator&,size_t)> func=[f,this,id](Generator& G,size_t depth=0)
    {
        int nbRetry=0;
        while(true)
        {
            try
            {
                return Call_Impl(f,G,depth,id,std::make_index_sequence<SizeArgument<typename FunctionOp::TypeFunctionNonML>::size>());
            }
            catch(...)
            {
                ++nbRetry;
                if(nbRetry>10)
                {
                    throw;
                }
            }
        }
    };
    m_funcList.push_back(func);
    return id;
}

template <class Generator>
template <class FunctionOp,class FuncGenerate>
size_t ExpressionGenerator<Generator>::FuncCreation(const FunctionOp& f,FuncGenerate funcGenerate)
{
    size_t id=m_funcList.size();
    std::function<Expressions(Generator&,size_t)> func=[f,funcGenerate,this,id](Generator& G,size_t depth=0)
    {
        int nbRetry=0;
        while(true)
        {
            try
            {
                return Call_Impl(f,funcGenerate,G,depth,id,std::make_index_sequence<SizeArgument<typename FunctionOp::TypeFunctionNonML>::size>());
            }
            catch(...)
            {
                ++nbRetry;
                if(nbRetry>10)
                {
                    throw;
                }
            }
        }
    };
    m_funcList.push_back(func);
    return id;
}

template <class Generator>
template<typename FunctionOp,size_t ... indices> 
auto ExpressionGenerator<Generator>::Call_Impl(const FunctionOp& f,Generator& G,size_t depth,size_t id,std::index_sequence<indices...> )
{
    Expressions expressions;
    Expression expr;
    expr.m_maxStack=0;
    expr.m_size=SizeArgument<typename FunctionOp::TypeFunctionNonML>::size;
    (Call_elem<indices,FunctionOp>(G,depth,expressions,expr), ...);
    expr.m_output=Symbolic_Variable::NewVariable();
    auto func=f.GetFunctionReal();
    auto tuple=GetTuple(func,expr.m_inputs[indices].Get()...);
    expr.m_output.Set(std::apply(func,tuple));
    expr.m_func=id;
    expr.m_stack=std::max(expr.m_size,static_cast<size_t>(1));
    expr.m_maxStack=std::max(expr.m_maxStack,expr.m_stack);
    expressions.push_back(expr);
    return expressions;
}

template <class Generator>
template<typename FunctionOp,class FuncGenerate,size_t ... indices> 
auto ExpressionGenerator<Generator>::Call_Impl(const FunctionOp& f,FuncGenerate funcgenerate,Generator& G,size_t depth,size_t id,std::index_sequence<indices...> )
{
    Expressions expressions;
    Expression expr;
    expr.m_maxStack=0;
    expr.m_size=SizeArgument<typename FunctionOp::TypeFunctionNonML>::size;
    (Call_elem<indices,FunctionOp>(G,depth,expressions,expr), ...);
    expr.m_output=Symbolic_Variable::NewVariable();
    auto func=f.GetFunctionReal();
    auto tuple=GetTuple(func,expr.m_inputs[indices].Get()...);
    tuple=funcgenerate(tuple,G,expr);
    expr.m_output.Set(std::apply(func,tuple));
    expr.m_func=id;
    expr.m_stack=std::max(expr.m_size,static_cast<size_t>(1));
    expr.m_maxStack=std::max(expr.m_maxStack,expr.m_stack);
    expressions.push_back(expr);
    return expressions;
}


template <class Generator>
template<size_t i,typename FunctionOp>
void ExpressionGenerator<Generator>::Call_elem(Generator& G,size_t depth,Expressions& exprs, Expression& expr)
{
    typedef typename TypeArgument<typename FunctionOp::TypeFunctionNonML,i>::type type;
    auto func=GetRandom(std::type_index(typeid(type)),G,depth+1);
    auto expressions=func(G,depth+1);
    expr.m_maxStack+=expressions.back().m_maxStack;
    expr.m_inputs.push_back(expressions.back().m_output);
    exprs.insert(exprs.end(),expressions.begin(),expressions.end());
}

template <class Generator>
auto ExpressionGenerator<Generator>::GetRandom(std::type_index type,Generator& G,size_t depth) ->std::function<Expressions (Generator&,size_t)>
{
    if(depth<m_max_depth)
    {
        auto elem=m_list.find(type);
        size_t count=m_list.count(type);
        std::uniform_int_distribution<int> distribution(0,count-1);
        int sample=distribution(G);
        std::advance(elem,sample);
        auto res=elem->second;
        return m_funcList[res];
    }
    else
    {
        auto elem=m_listDepth.find(type);
        size_t count=m_listDepth.count(type);
        std::uniform_int_distribution<int> distribution(0,count-1);
        int sample=distribution(G);
        std::advance(elem,sample);
        auto res=elem->second;
        return m_funcList[res];
    }
}

template <class Generator>
template<typename Type>
auto ExpressionGenerator<Generator>::GetRandom(Generator& G,size_t depth)->std::function<Expressions (Generator&,size_t)>
{
    std::function<Expressions (Generator&,size_t)> res=[&G,depth,this](Generator& G2,size_t depth2)
    {
        while(true)
        {
            try
            {
                return GetRandom(std::type_index(typeid(Type)),G,depth)(G2,depth2);
            }
            catch(...)
            {
            }
        }
    };
    return res;
}

template <class Generator>
template<typename Result>
size_t ExpressionGenerator<Generator>::InsertGenerator(std::function<Result(Generator&,size_t)> f,bool depth)
{
    std::type_index index(typeid(Result));
    size_t func=FuncCreationGenerator(f);
    m_list.insert({index,func});
    if(depth)
    {
        m_listDepth.insert({index,func});
    }
    return func;
}

template <class Generator>
template <class Result>
size_t ExpressionGenerator<Generator>::FuncCreationGenerator(const std::function<Result(Generator&,size_t)>& f)
{
    size_t id=m_funcList.size();
    std::function<Expressions(Generator&,size_t)> func=[f,this,id](Generator& G,size_t depth=0)
    {
        Expression exp;
        exp.m_output=Symbolic_Variable::NewVariable();
        exp.m_output.Set(f(G,depth));
        exp.m_func=id;
        Expressions ret{exp};
        return ret;
    };
    m_funcList.push_back(func);
    return id;
}

template <class Generator>
template <typename Result,typename ...Args,typename ...Any>
auto ExpressionGenerator<Generator>::GetTuple(const std::function<Result(Args...)> & func,Any... data)
{
    return std::make_tuple(std::any_cast<Args>(data) ...);
}

