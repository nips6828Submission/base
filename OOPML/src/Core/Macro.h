#pragma once
#ifndef SERIALIZATIONS
    /**
     * @brief Macro which should be added to all classes. It is meant to be remplaced by friendship options.
     */
    #define SERIALIZATIONS
#endif
