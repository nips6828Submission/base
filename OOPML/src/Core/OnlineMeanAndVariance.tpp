
template <class ValueType>
OnlineMeanAndVariance<ValueType>::OnlineMeanAndVariance():m_mean(),m_M2()
{

};

template <class ValueType>
OnlineMeanAndVariance<ValueType>::OnlineMeanAndVariance(ValueType mean, ValueType variance,int n):m_n(n),m_mean(mean),m_M2(variance*(n-1)),m_Variance(variance)
{
};

template <class ValueType>
void OnlineMeanAndVariance<ValueType>::Add(ValueType value)
{
    if(m_n==0)
    {
	m_mean=value;
	++m_n;
	m_M2=0*m_mean;
    }
    else
    {
	++m_n;
	ValueType delta=value-m_mean;
	m_mean+=delta*(1/double(m_n));
	ValueType delta2=value-m_mean;
	m_M2+=(delta*delta2);
	m_Variance=m_M2*(1/double(m_n-1));
    }
};

template <class ValueType>
const ValueType& OnlineMeanAndVariance<ValueType>::GetMean() const
{
    return m_mean;
};

template <class ValueType>
const ValueType& OnlineMeanAndVariance<ValueType>::GetVariance() const
{
    return m_Variance;
};

template <class ValueType>
int OnlineMeanAndVariance<ValueType>::GetN() const
{
    return m_n;
};

