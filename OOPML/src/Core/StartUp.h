#include "Core/Pytorch.h"
#include <iostream>

/**
 * @brief Class which should be instantiated at global scope before including other file.
 * This class ensure that pytorch is correctly initialized.
 */
class StartUp
{
    Pytorch pytorch;
public:
    StartUp()
    {
    }
};
