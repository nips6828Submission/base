#pragma once

#include <string>
#include <iostream>  
#include <memory>
#include <any>
#include <boost/fusion/include/adapt_struct.hpp>

/**
 * @param Symbolic_Variable represent a symbolic variable similar to the torch class of the same name.
 */
class Symbolic_Variable
{
    friend struct boost::fusion::extension::access;
    /**
     * @brief Pointer on the type.
     */
    std::shared_ptr<std::any> m_pointer;
public:
    /**
     * @brief Default constructor.
     */
    Symbolic_Variable():m_pointer(nullptr)
    {
    }
    /**
     * @brief Construct from and existing pointer.
     */
    Symbolic_Variable(std::shared_ptr<std::any> pointer):m_pointer(pointer)
    {
    }
    
    /**
     * @brief Construct from and existing variable.
     */
    Symbolic_Variable(const Symbolic_Variable& var):m_pointer(var.m_pointer)
    {
    }
    /**
     * @brief Return a new symbolic variable.
     */
    static Symbolic_Variable NewVariable()
    {
        return Symbolic_Variable(std::make_shared<std::any>());
    }
    /**
     * @brief Check if both Symbolic variable are the same.
     */
    bool operator==(Symbolic_Variable other) const
    {
        return m_pointer==other.m_pointer;
    }
    /**
     * @brief Check if both Symbolic variable are different.
     */
    bool operator!=(Symbolic_Variable other) const
    {
        return m_pointer!=other.m_pointer;
    }
    
    void Set(std::any value)
    {
        *m_pointer=value;
    }
    std::any Get()
    {
        return *m_pointer;
    }
    std::shared_ptr<std::any> GetPointer()
    {
        return m_pointer;
    }
    /**
     * @brief Output to the stream.
     */
    friend std::ostream& operator<<(std::ostream& os, const Symbolic_Variable& var);
};

std::ostream& operator<<(std::ostream& os, const Symbolic_Variable& var)
{
    os<<var.m_pointer;
    return os;
};

BOOST_FUSION_ADAPT_STRUCT(Symbolic_Variable,m_pointer)
