#include <torch/csrc/utils/auto_gil.h>
#include <torch/csrc/autograd/engine.h>
#include <torch/csrc/autograd/python_engine.h>

inline void Backward(torch::autograd::Variable& tensor,torch::autograd::Variable& grad)
{
    typedef std::vector<torch::autograd::Variable> variable_list;
    typedef std::vector<torch::autograd::Edge> edge_list;
    edge_list roots;
    roots.reserve(1);
    variable_list grads;
    grads.reserve(1);
    roots.push_back(std::move(tensor.gradient_edge()));
    grads.push_back(grad);
    std::vector<torch::autograd::Edge> output_edges;
    variable_list outputs;
    {
        AutoNoGIL no_gil;
        outputs = torch::autograd::Engine::get_default_engine().execute(roots, grads, false, false, output_edges);
    }
}
