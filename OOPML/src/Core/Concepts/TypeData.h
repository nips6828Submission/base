#pragma once

template<typename T>
concept bool CTypeData = requires(T a)
{
    { a.size() } -> size_t;
}
;
