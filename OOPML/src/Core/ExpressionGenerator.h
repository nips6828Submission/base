#pragma once
#include "Expression.h"
#include <functional>
#include <tuple>
/**
 * @brief ExpressionGenerator generate functional expression using a list of functions.
 * @tparam Generator Random number generator to use.
 * 
 * The use case of this class is the following:
 * - The caller register different functions with Insert and InsertGeneration.
 * - The caller recover an expressions with GetRandom.
 * 
 * This class is implemented in the following maner:
 * - The insert functions create a functor which will be stored into a map m_funcList.
 * - At usage the call of the functor recursively call other function inside the list.
 */
template <class Generator>
class ExpressionGenerator
{
    /**
     * @brief Type of an expressions.
     */
    typedef std::deque<Expression> Expressions;
    /**
     * @brief Type of a list of inputs.
     */
    typedef std::deque<Symbolic_Variable> Inputs;
    
    /**
     * @brief Max Id;
     */
    size_t m_maxId=0;
    /**
     * @brief List of function indexed by results.
     */
    std::multimap<std::type_index,size_t> m_list;
    /**
     * @brief List of functions indexed by results with the depth property.
     */
    std::multimap<std::type_index,size_t> m_listDepth;
    /**
     * @brief List of all functions.
     */
    std::deque<std::function<Expressions(Generator&,size_t)>> m_funcList;
    
    /**
     * @brief Maximum depth when only the shortest path is used.
     */
    size_t m_max_depth=10;
    
    /**
     * @brief auxiliary function which Create and push the needed function into the list.
     * @return Id of the function we just added into the list.
     */
    template <class FunctionOp>
    size_t FuncCreation(const FunctionOp& f);
    
    /**
     * @brief auxiliary function which Create and push the needed function into the list.
     * @return Id of the function we just added into the list.
     */
    template <class FunctionOp,class FuncGenerate>
    size_t FuncCreation(const FunctionOp& f,FuncGenerate funcGenerate);
    
    /**
     * @brief auxiliary function which Create and push the needed function into the list.
     * @return Id of the function we just added into the list.
     */
    template <class Result>
    size_t FuncCreationGenerator(const std::function<Result(Generator&,size_t)>& f);
    
    /**
     * @brief Implementation function which assemble the final expressions.
     * @tparam FunctionOp Type of the functionOp.
     * @tparam indices List of indices to treat.
     * @param f Function to treat.
     * @param G Random number generator to use.
     * @param depth Current depth.
     * @param id Id of the function.
     */
    template<typename FunctionOp,size_t ... indices> 
    auto Call_Impl(const FunctionOp& f,Generator& G,size_t depth,size_t id,std::index_sequence<indices...> );
    
    /**
     * @brief Implementation function which assemble the final expressions.
     * @tparam FunctionOp Type of the functionOp.
     * @tparam indices List of indices to treat.
     * @param f Function to treat.
     * @param G Random number generator to use.
     * @param depth Current depth.
     * @param id Id of the function.
     */
    template<typename FunctionOp,class FuncGenerate,size_t ... indices> 
    auto Call_Impl(const FunctionOp& f,FuncGenerate funcgenerate,Generator& G,size_t depth,size_t id,std::index_sequence<indices...> );

    /**
     * @brief Add the expressions of a specific function input. And update the needed structure.
     * @tparam FunctionOp Type of the functionOp.
     * @tparam i Which input number to treat.
     * @param G Random number generator to use.
     * @param depth Current depth.
     * @param exprs Current list of expression, it will be updated to contain the new expression.
     * @param expr Current expression, the input with be updated to contain the result of the previous function call.
     */
    template<size_t i,typename FunctionOp>
    void Call_elem(Generator& G,size_t depth,Expressions& exprs, Expression& expr);
    
    /**
     * @brief Function which obtain randomly from the list a function generating an expressions of the asked type.
     * @param type Type variable which we want to obtain an expressions.
     * @param G Random number generator to use.
     * @param depth Current depth.
     */
    std::function<Expressions (Generator&,size_t)> GetRandom(std::type_index type,Generator& G,size_t depth=0);
    
    template <typename Result,typename ...Args,typename ...Any>
    auto GetTuple(const std::function<Result(Args...)> & func,Any ... data);
    
public:
    typedef Generator type_Generator;
    /**
     * @brief Default constructor.
     */
    ExpressionGenerator();
    /**
     * @brief Setter for depth.
     */
    void SetMaxDepth(size_t depth)
    {
        m_max_depth=depth;
    }
    
    size_t GetMaxDepth() const
    {
        return m_max_depth;
    }
    /**
     * @brief Insert a function to the generator.
     * @return Id indicating the function we just added.
     */
    template <class FunctionOp>
    size_t Insert(const FunctionOp& f,bool depth=false);
    
    /**
     * @brief Insert a function to the generator.
     * @return Id indicating the function we just added.
     */
    template <class FunctionOp,class FuncGenerate>
    size_t Insert(const FunctionOp& f,FuncGenerate funcGenerate,bool depth=false);
    
    /**
     * @brief Insert a function generator to the generator.
     */
    template<typename Result>
    size_t InsertGenerator(std::function<Result(Generator&,size_t)> f,bool depth=false);
    
    /**
     * @brief Function which obtain randomly from the list a function generating an expressions of the asked type.
     * @tparam Type Type which we want to obtain an expression.
     * @param G Random number generator to use.
     * @param depth Current depth.
     */
    template<typename Type>
    std::function<Expressions (Generator&,size_t)> GetRandom(Generator& G,size_t depth=0);
    
};

#include "ExpressionGenerator.tpp"
