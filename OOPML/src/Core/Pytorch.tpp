#include "Core/Pytorch.h"
#include <boost/python/numpy.hpp>
#include <iostream>

/**
 * This code block will create a new built in module MyTorch_C which is equivalent to the original torch._C but without loading at runtime the shared library.
 * On te contrary we statically link the dynamical shared library and pass the entry point to python as builtin. This allows to use the backend in c++ code.
 *
 **/
inline Pytorch::Pytorch()
{
    try
    {
    //Pass the torch backend to python as MyTorch_C.
    //PyImport_AppendInittab("MyTorch_C", &PyInit__Test);
    Py_Initialize();
    boost::python::object main = boost::python::import("__main__");
    boost::python::object global(main.attr("__dict__"));
    //Python code which trick python to believe torch._C module was already imported and pass it the builtin MyTorch_C.
    //Then finally import torch.
    boost::python::exec(
    "import torch\n"
    "import torch.jit.batchop",global,global);
    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}
