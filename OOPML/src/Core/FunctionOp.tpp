#include "Core/FunctionOp.h"
#include "Core/Function.h"
#include "torch/csrc/jit/passes/to_batch.h"
#include "torch/csrc/jit/script/compiler.h"

template <typename FunctionType>
FunctionOp<FunctionType>::FunctionOp(std::shared_ptr<torch::jit::Graph> graph,std::shared_ptr<torch::jit::Graph> graphNoBatch,torch::jit::Stack param):m_impl(graph,graphNoBatch,param),m_func(GetLambda(*this))
{
}

template <typename FunctionType>
FunctionOp<FunctionType>::FunctionOp(std::shared_ptr<torch::jit::Graph> graph,std::shared_ptr<torch::jit::Graph> graphNoBatch,torch::jit::Stack param,const TypeFunctionNonML& funcReal):m_impl(graph,graphNoBatch,param),m_func(GetLambda(*this)),m_funcReal(funcReal)
{
}



FunctionOp<void>::FunctionOp(std::shared_ptr<torch::jit::Graph> graph,std::shared_ptr<torch::jit::Graph> graphNoBatch,torch::jit::Stack param):m_impl(graph,graphNoBatch,param)
{
}

FunctionOp_Impl::FunctionOp_Impl(std::shared_ptr<torch::jit::Graph> graph,std::shared_ptr<torch::jit::Graph> graphNoBatch,torch::jit::Stack param):m_graph(graph),m_noBatchGraph(graphNoBatch),m_params(param),m_executor(graphNoBatch,true)
{
}

std::vector<torch::jit::SymbolicVariable> FunctionOp_Impl::InsertNode(torch::jit::Graph* graph,torch::jit::ArrayRef<torch::jit::SymbolicVariable> symbParams,torch::jit::ArrayRef<torch::jit::SymbolicVariable> inputs)
{
    std::vector<torch::jit::Value*> input;
    for(auto i : inputs)
    {
        input.emplace_back(i);
    }
    for(auto i : symbParams)
    {
        input.emplace_back(i);
    }
    auto res=torch::jit::inlineCallTo(*graph,*m_graph, input);
    std::vector<torch::jit::SymbolicVariable> out;
    for(auto v : res)
    {
        out.push_back(v);
    }
    return out;
}

std::vector<torch::jit::SymbolicVariable> FunctionOp_Impl::CreateSymbolicParameters(torch::jit::Graph* graph)
{
    std::vector<torch::jit::SymbolicVariable> out;
    for(size_t i=0;i<m_params.size();++i)
    {
        out.emplace_back(torch::jit::SymbolicVariable::asNewInput(*graph));
    }
    return out;
}

void FunctionOp_Impl::Load(torch::serialize::InputArchive& archive)
{
    int i=0;
    for(auto& elem : m_params)
    {
        at::Tensor tensor;
        archive.read(std::to_string(i),tensor);
        elem=tensor.type_as(elem.toTensor());
        ++i;
    }
}

torch::serialize::OutputArchive FunctionOp_Impl::Save() const
{
    torch::serialize::OutputArchive output;
    int i=0;
    for(auto& elem : m_params)
    {
        auto tensor=elem.toTensor().cpu();
        output.write(std::to_string(i),tensor);
        ++i;
    }
    return output;
}

template <typename Return,typename... Arg>
std::function<Return(Arg...)> GetLambda(FunctionOp<std::function<Return(Arg...)>>& func)
{
    return [&func](Arg ... args) -> Return
    {
        torch::jit::Stack stack;
        (stack.push_back(args),...);
        stack.insert(stack.end(),func.m_impl.GetParamRef().begin(),func.m_impl.GetParamRef().end());
        func.m_impl.GetGraphExecutor().run(stack);
        //if constexpr (std::is_convertible<at::Tensor,Return>::value)
        {
            return static_cast<Return>(stack[0].toTensor());
        }
        /*else if (std::is_same<Return,void>::value)
        {
            if(stack.size()!=1)
            {
                throw std::runtime_error("stack is not of size 0 in " __FILE__ "__LINE__");
            }
        }
        else
        {
            return stack;
        }*/
    };
}

template <typename FunctionType>
FunctionOp<void> FunctionOp<FunctionType>::GetGeneric() const
{
    return FunctionOp<void>(m_impl);
};



