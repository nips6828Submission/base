#pragma once

#include "ExperimentData.h"
#include "Concepts/TypeData.h"
#include "Done.h"
#include "Data.h"
#include <iostream>
#include <boost/filesystem.hpp>

/**
 * @brief Generic class to store an experiment and its metadata.
 * @tparam ExperimentDataType Type of the data.
 */
template <CExperimentData ExperimentDataType>
class Experiment : public Done
{
    /**
     * @brief Name of the experiment.
     */
    std::string m_name;
    /**
     * @brief Data of the experiment.
     */
    ExperimentDataType m_experimentData;
protected:
    /**
     * @brief Constructor for Data with only a name.
     * @arg name Name of the experiment.
     * @arg dataName Name of the data.
     */
    Experiment(std::string name,std::string dataName): m_name(name),m_experimentData(dataName)
    {
    }
    /**
     * @brief Constructor for Generic data.
     * @arg name Name of the ExperimentData.
     * @arg args Other argument, will be passed to the Experiment Data constructor.
     * @tparam ...Args Type of inputs for the Experiment Data constructor.
     */
    template<typename ...Args>
    Experiment(std::string name,Args... args): m_name(name),m_experimentData(args...)
    {
    }
    
public:
    /**
     * @brief Return the Name of the experiment.
     * @return Name.
     */
    virtual std::string GetName() const
    {
        return m_name;
    }
    /**
     * @brief Return the Data.
     * @return Data.
     */
    virtual ExperimentDataType& GetData()
    {
        return m_experimentData;
    }
    /**
     * @brief Return the name of the Data.
     * @return DataName.
     */
    virtual std::string GetDataName() const
    {
        return m_experimentData.GetDataName();
    }
    /**
     * @brief Print description of the data and experiment.
     */
    virtual void PrintDescription() const
    {
        std::cout<<"name: "<<GetName()<<std::endl;
        std::cout<<"nameData: "<<GetDataName()<<std::endl;
    }
    /**
     * @brief Obtain the path of the directory of the experiment.
     */
    virtual std::string GetDirectoryPath() const=0;
    /**
     * @brief Check if the experiment is already done.
     */
    virtual bool AlreadyDone()
    {
        if(Done::AlreadyDone())
        {
            return true;
        }
        else
        {
            if(boost::filesystem::exists(boost::filesystem::path{GetDirectoryPath()+std::string("/done")}))
            {
                Done::SetDone();
                return true;
            }
        }
        return false;
    };
    /**
     * @brief Set the experiment as done.
     */
    virtual void SetDone()
    {
        boost::filesystem::create_directories(boost::filesystem::path{GetDirectoryPath()});
        std::ofstream oftouch(GetDirectoryPath()+std::string("/done"));
        oftouch<<"done"<<std::endl;
    };
};

