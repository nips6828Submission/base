#include "ML.h"
#include <cfenv>

template <typename Class,typename Enable>
ML<Class,Enable>::ML()
{
    
}

template <typename Class,typename Enable>
ML<Class,Enable>::ML(at::Tensor embedded):at::Tensor(embedded)
{
}

template <typename Class,typename Enable>
size_t ML<Class,Enable>::size()
{
    return 2048;
}

template <typename Class,typename Enable>
constexpr bool ML<Class,Enable>::IsFloatCompatible()
{
    return false;
}

template <typename FloatType>
ML<FloatType,std::enable_if_t<std::is_floating_point<FloatType>::value>>::ML()
{
    
}

template <typename FloatType>
ML<FloatType,std::enable_if_t<std::is_floating_point<FloatType>::value>>::ML(at::Tensor embedded):at::Tensor(embedded)
{
}

template <typename FloatType>
ML<FloatType,std::enable_if_t<std::is_floating_point<FloatType>::value>>::operator FloatType const()
{
    return at::native::item((*this)[0]).toDouble();
}

template <typename FloatType>
size_t ML<FloatType,std::enable_if_t<std::is_floating_point<FloatType>::value>>::size()
{
    return 1;
}

template <typename FloatType>
constexpr bool ML<FloatType,std::enable_if_t<std::is_floating_point<FloatType>::value>>::IsFloatCompatible()
{
    return true;
}

template <typename IntegerType>
ML<IntegerType,std::enable_if_t<std::is_integral<IntegerType>::value>>::ML()
{
    
}

template <typename IntegerType>
ML<IntegerType,std::enable_if_t<std::is_integral<IntegerType>::value>>::ML(at::Tensor embedded):at::Tensor(embedded)
{
}

template <typename IntegerType>
ML<IntegerType,std::enable_if_t<std::is_integral<IntegerType>::value>>::operator IntegerType const()
{
    return at::native::item((*this)[0]).toInt();
}

template <typename IntegerType>
size_t ML<IntegerType,std::enable_if_t<std::is_integral<IntegerType>::value>>::size()
{
    return 1;
}

template <typename IntegerType>
constexpr bool ML<IntegerType,std::enable_if_t<std::is_integral<IntegerType>::value>>::IsFloatCompatible()
{
    return true;
}
