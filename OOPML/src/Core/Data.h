#pragma once
#include "Concepts/TypeData.h"

/**
 * @brief Template class used to store Train,Test and Validation data.
 * @tparam TypeData Type of each dataset.
 */
template <CTypeData TypeData>
class Data
{
    /**
     * @brief Training Data.
     */
    TypeData m_trainData;
    /**
     * @brief Testing Data.
     */
    TypeData m_testData;
    /**
     * @brief Validation Data.
     */
    TypeData m_valData;
public:
    /**
     * @brief Accessor to the training Data.
     * @return A reference to the training data.
     */
    virtual TypeData& TrainData()
    {
        return m_trainData;
    };
    /**
     * @brief Accessor to the testing Data.
     * @return A reference to the testing data.
     */
    virtual TypeData& TestData()
    {
        return m_testData;
    };
    /**
     * @brief Accessor to the validation Data.
     * @return A reference to the validation data.
     */
    virtual TypeData& ValData()
    {
        return m_valData;
    };
    /**
     * @brief Setter of the training data.
     * @arg data Data to set to.
     */
    virtual void SetTrainData(const TypeData& data)
    {
        m_trainData=data;
    };
    /**
     * @brief Setter of the testing data.
     * @arg data Data to set to.
     */
    virtual void SetTestData(const TypeData& data)
    {
        m_testData=data;
    };
    /**
     * @brief Setter of the validation data.
     * @arg data Data to set to.
     */
    virtual void SetValData(const TypeData& data)
    {
        m_valData=data;
    };
    /**
     * @brief Print a description of the datasets.
     */
    virtual void PrintDescription() const
    {
        std::cout<<"Train Size: "<<m_trainData.size()<<std::endl;
        std::cout<<"Test Size: "<<m_testData.size()<<std::endl;
        std::cout<<"Val Size: "<<m_valData.size()<<std::endl;
    }
    /**
     * @brief Serializer of the dataset.
     * @arg ar Archive to write from, to.
     * @arg version Version number, currently not used.
     */
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        ar & m_trainData;
        ar & m_testData;
        ar & m_valData;
    };
};

