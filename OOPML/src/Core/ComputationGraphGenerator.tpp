#include "ComputationGraphGenerator.h"
#include "Core/ML.h"
#include "Core/ExpressionsToGraph.h"
#include <boost/graph/graphml.hpp>
#include <fstream>

template <typename TypeExpressionGenerator>
ComputationGraphGenerator<TypeExpressionGenerator>::ComputationGraphGenerator(size_t stackSize):m_stackSize(stackSize)
{
};

template <typename TypeExpressionGenerator>
template <class FunctionOp,class FunctionGen>
void ComputationGraphGenerator<TypeExpressionGenerator>::Insert(const FunctionOp& f,const FunctionGen& g,bool depth)
{
    torch::jit::Stack param;
    auto Param=f.GetParam();
    param.insert(param.end(),Param.begin(),Param.end());
    std::transform(param.begin(),param.end(),std::back_inserter(m_paramTensor),[](auto elem)
    {
        return elem.toTensor();
    });
    size_t id=m_generator.Insert(f,g,depth);
    m_functionMap[id]=f.GetGeneric();
    m_ReturnFunc.insert(std::make_pair(id,GetReturnIndex(f.GetFunction())));
    m_functionStackUsage[id]=GetStackUsage(f.GetFunction());
    UpdateType(f.GetFunction());
    Type_Generator G;
    ++m_nbFunc;
}

template <typename TypeExpressionGenerator>
template <class FunctionOp>
void ComputationGraphGenerator<TypeExpressionGenerator>::Insert(const FunctionOp& f,bool depth)
{
    torch::jit::Stack param;
    auto Param=f.GetParam();
    param.insert(param.end(),Param.begin(),Param.end());
    std::transform(param.begin(),param.end(),std::back_inserter(m_paramTensor),[](auto elem)
    {
        return elem.toTensor();
    });
    size_t id=m_generator.Insert(f,depth);
    m_functionMap[id]=f.GetGeneric();
    m_ReturnFunc.insert(std::make_pair(id,GetReturnIndex(f.GetFunction())));
    m_functionStackUsage[id]=GetStackUsage(f.GetFunction());
    UpdateType(f.GetFunction());
    Type_Generator G;
    ++m_nbFunc;
}

template <typename TypeExpressionGenerator>
template <class FunctionOp>
void ComputationGraphGenerator<TypeExpressionGenerator>::InsertLoss(const FunctionOp& f)
{
    m_lossMap.insert(std::make_pair(GetReturnLossIndex(f.GetFunction()),f.GetGeneric()));
}

template <typename TypeExpressionGenerator>
template<typename Result>
void ComputationGraphGenerator<TypeExpressionGenerator>::InsertGenerator(std::function<Result(Type_Generator&,size_t)> f,bool depth)
{
    size_t id=m_generator.InsertGenerator(f,depth);
    m_TypeReturn.insert(std::type_index(typeid(Result)));
    m_TypeGen.insert(std::type_index(typeid(Result)));
    m_GeneratorFunc.insert(std::make_pair(id,std::type_index(typeid(Result))));
    std::function<void(std::any,at::TensorAccessor<float, 1>&)> funcGen=[](std::any input,at::TensorAccessor<float, 1>& tensor)
    {
        GenerateMLValue(std::any_cast<Result>(input),tensor);
    };
    m_anyToTensor[std::type_index(typeid(Result))]=funcGen;
    m_TypeSize[typeid(Result)]=ML<Result>::size();
    ++m_nbFunc;
}

template <typename TypeExpressionGenerator>
template <typename Arg>
void ComputationGraphGenerator<TypeExpressionGenerator>::CountUsage(std::vector<std::pair<std::type_index,size_t>>& count,std::map<std::type_index,size_t>& ArgCount)
{
    count.push_back(std::make_pair(std::type_index(typeid(Arg)),ArgCount[std::type_index(typeid(Arg))]));
    ++ArgCount[std::type_index(typeid(Arg))];
}

template <typename TypeExpressionGenerator>
template<typename Return,class ...Args>
std::type_index ComputationGraphGenerator<TypeExpressionGenerator>::GetReturnIndex(const std::function<Return(Args...)>& func)
{
    return std::type_index(typeid(typename Return::Type));
}

template <typename TypeExpressionGenerator>
template<typename Return,class Arg>
std::type_index ComputationGraphGenerator<TypeExpressionGenerator>::GetReturnLossIndex(const std::function<Return(Arg,Arg)>& func)
{
    typedef typename Arg::Type Loss_Type;
    std::function<void(std::any,at::TensorAccessor<float, 1>&)> funcGen=[](std::any input,at::TensorAccessor<float, 1>& tensor)
    {
        GenerateMLValue(std::any_cast<Loss_Type>(input),tensor);
    };
    m_anyToTensor[std::type_index(typeid(Loss_Type))]=funcGen;
    return std::type_index(typeid(typename Arg::Type));
}

template <typename TypeExpressionGenerator>
template<typename Return,class ...Args>
auto ComputationGraphGenerator<TypeExpressionGenerator>::GetStackUsage(const std::function<Return(Args...)>& func) -> Type_Function
{
    std::vector<std::pair<std::type_index,size_t>> count;
    std::map<std::type_index,size_t> ArgCount;
    (CountUsage<typename Args::Type>(count,ArgCount) , ...);
    auto funcRet=[count,ArgCount,this](auto stack)
    {
        std::vector<torch::jit::SymbolicVariable> StackRes;
        std::map<std::type_index,torch::jit::SymbolicVariable> stackAfter=stack;
        std::for_each(count.begin(),count.end(),[&stack,&StackRes,this](auto elem)
        {
            auto variable=stack.at(elem.first);
            auto one=variable.value()->owningGraph()->insertConstant(1);
            auto column=variable.value()->owningGraph()->insertConstant(static_cast<int>(elem.second));
            auto select=variable.create(torch::jit::aten::select,{variable,one,column});
            StackRes.push_back(select[0]);
        });
        std::for_each(ArgCount.begin(),ArgCount.end(),[&stackAfter,&stack,this](auto elem)
        {
            auto variable=stack.at(elem.first);
            auto one=variable.value()->owningGraph()->insertConstant(1);
            auto res=variable.narrow(1,elem.second,m_stackSize-elem.second);
            auto oldVar=variable.narrow(1,0,elem.second+1);
            stackAfter[elem.first]=torch::jit::SymbolicVariable::cat({res,oldVar},one);
            return stackAfter;
        });
        return std::make_pair(StackRes,stackAfter);
    };
    return funcRet;
}

template <typename TypeExpressionGenerator>
template<typename Return,class ...Args>
void ComputationGraphGenerator<TypeExpressionGenerator>::UpdateType(const std::function<Return(Args...)>& func)
{
    m_TypeReturn.insert(std::type_index(typeid(typename Return::Type)));
    m_TypeSize[typeid(typename Return::Type)]=Return::size();
}

template <typename TypeExpressionGenerator>
void ComputationGraphGenerator<TypeExpressionGenerator>::CreateGraphExecutor()
{
    std::for_each(m_lossMap.begin(),m_lossMap.end(),[this](auto typeLoss)
    {
    auto graph=std::make_shared<torch::jit::Graph>();
    //Define inputs
    torch::jit::SymbolicVariable Size = torch::jit::SymbolicVariable::asNewInput(*graph);
    torch::jit::SymbolicVariable Call = torch::jit::SymbolicVariable::asNewInput(*graph);
    
    //Map from index to symbolicVariable used to store result from Generator.
    std::map<std::type_index,torch::jit::SymbolicVariable> Gen;
    std::for_each(m_TypeGen.begin(),m_TypeGen.end(),[&graph,&Gen](auto elem)
    {
        Gen[elem]=torch::jit::SymbolicVariable::asNewInput(*graph);
    });
    
        //Map from index to symbolicVariable used to store result from Generator.
    std::map<std::type_index,torch::jit::SymbolicVariable> Result;
    std::for_each(m_lossMap.begin(),m_lossMap.end(),[&graph,&Result](auto elem)
    {
        Result[elem.first]=torch::jit::SymbolicVariable::asNewInput(*graph);
    });
    
    torch::jit::SymbolicVariable Error = torch::jit::SymbolicVariable::asNewInput(*graph);
    
    int num_loop=0;
    
    //Map of Type to the stack for the given type.
    std::map<std::type_index,torch::jit::SymbolicVariable> InputStack;
    // Fill the stack
    std::for_each(m_TypeReturn.begin(),m_TypeReturn.end(),[&InputStack,&graph,this,&num_loop](auto elem)
    {
        InputStack[elem]=torch::jit::SymbolicVariable::asNewInput(*graph);
        ++num_loop;
    }
    );
    
    //Map of all parameter symbolic variable.
    std::map<size_t,std::vector<torch::jit::SymbolicVariable>> ParamVar;
    std::for_each(m_functionMap.begin(),m_functionMap.end(),[&ParamVar,&graph](auto elem)
    {
        ParamVar[elem.first]=elem.second.CreateSymbolicParameters(graph);
    });
    
    //Misceleanous usefull constant.
    auto negoneConstant=graph->insertConstant(-1);
    auto oneConstant=graph->insertConstant(1);
    auto zeroConstant=graph->insertConstant(0);
    auto MaxLoop=graph->insertConstant(at::ones({},at::kCUDA)*10000);
    //Boolean indicating if we need to loop.
    auto cond=Size>torch::jit::SymbolicVariable(negoneConstant);
    
    //Main Loop.
    auto loop=graph->insertNode(graph->create(torch::jit::prim::Loop,num_loop+1));
    loop->addInput(MaxLoop);
    loop->addInput(cond);
    loop->addInput(Error);
    //The loop has a dynamic Input stack.
    std::for_each(InputStack.begin(),InputStack.end(),[&loop](auto elem)
    {
        loop->addInput(elem.second);
    }
    );
    {
        auto body=loop->addBlock();
        auto counter=body->insertInput(0);
        auto error=body->insertInput(1);
        int num=2;
        //Map of the symbolic variable and stack. This is the same than InputStack but inside the for.
        std::map<std::type_index,torch::jit::SymbolicVariable> InputStackIn;
        //Fill the Map.
        std::for_each(m_TypeReturn.begin(),m_TypeReturn.end(),[&body,&InputStackIn,&num,this](auto elem)
        {
            InputStackIn[elem]=body->insertInput(num);
            ++num;
        }
        );
        int numStack=num-1;
        torch::jit::WithInsertPoint guard(body);
        //Select the Call input for the current iteration.
        auto select=body->appendNode(graph->create(torch::jit::aten::select,1));
        select->addInput(Call);
        select->addInput(oneConstant);
        select->addInput(counter);
        torch::jit::SymbolicVariable output_select=select->outputs()[0];
        //Main recursive function.
        //It recurse on every function.
        std::function<torch::jit::Node* (size_t i,torch::jit::Block*,torch::jit::SymbolicVariable,std::map<std::type_index,torch::jit::SymbolicVariable>)> CallRecursion=[&](size_t i,torch::jit::Block* body,torch::jit::SymbolicVariable errorInCall,std::map<std::type_index,torch::jit::SymbolicVariable> StackInCall)
        {
            //Function to add the call condition.
            auto ApplyCall=[&](auto body,torch::jit::SymbolicVariable errorInCall,std::map<std::type_index,torch::jit::SymbolicVariable> StackInCall)
            {
                torch::jit::WithInsertPoint guard(body);
                //Check if the function in question is a Generator
                bool isGenerator=(m_GeneratorFunc.count(i)>0);
                if(isGenerator)
                {
                    //For generator there is no call the output is simply obtained from the input.
                    std::type_index type(m_GeneratorFunc.at(i));
                    auto selectGen=body->appendNode(graph->create(torch::jit::aten::select,1));
                    selectGen->addInput(Gen[type]);
                    selectGen->addInput(oneConstant);
                    selectGen->addInput(counter);
                    auto output_selectGen=selectGen->outputs()[0];
                    auto output_selectGenNew=static_cast<torch::jit::SymbolicVariable>(output_selectGen).unsqueeze(1);
                    //Add the result to the stack
                    auto variable=StackInCall[type];
                    auto newvariable=torch::jit::SymbolicVariable::cat({output_selectGenNew,variable},oneConstant);
                    StackInCall[type]=newvariable.narrow(1,0,m_stackSize);
                    //Pop the end of the stack to maintain the same stack size.
                    if(m_lossMap.count(type)>0)
                    {
                        auto selectResult=body->appendNode(graph->create(torch::jit::aten::select,1));
                        selectResult->addInput(Result[type]);
                        selectResult->addInput(oneConstant);
                        selectResult->addInput(counter);
                        auto output_selectResult=selectResult->outputs()[0];
                        auto funcLoss=m_lossMap.at(type);
                        torch::jit::SymbolicVariable newError=funcLoss.InsertNode(graph,{},{output_selectGen,output_selectResult})[0];
                        errorInCall=errorInCall+newError;
                    }
                }
                else
                {
                    //Get the function graph.
                    auto Op=m_functionMap.at(i);
                    //Obtain the parameter and input and call the function.
                    auto [Input,NewStack]=m_functionStackUsage.at(i)(StackInCall);
                    auto result=Op.InsertNode(body->owningGraph(),ParamVar[i],Input)[0];
                    auto returnType=m_ReturnFunc.at(i);
                    auto variable=NewStack[returnType];
                    auto resultNew=result.unsqueeze(1);
                    auto newvariable=torch::jit::SymbolicVariable::cat({resultNew,variable},oneConstant);
                    StackInCall[returnType]=newvariable.narrow(1,0,m_stackSize);
                    if(m_lossMap.count(returnType)>0)
                    {
                        auto selectResult=body->appendNode(graph->create(torch::jit::aten::select,1));
                        selectResult->addInput(Result[returnType]);
                        selectResult->addInput(oneConstant);
                        selectResult->addInput(counter);
                        auto output_selectResult=selectResult->outputs()[0];
                        auto funcLoss=m_lossMap.at(returnType);
                        torch::jit::SymbolicVariable newError=funcLoss.InsertNode(graph,{},{result,output_selectResult})[0];
                        errorInCall=errorInCall+newError;
                    }
                }
                body->registerOutput(errorInCall);
                std::for_each(StackInCall.begin(),StackInCall.end(),[&body,this](auto elem)
                {
                    body->registerOutput(elem.second);
                });
            };
            auto ApplyIdentity=[&](auto body,torch::jit::SymbolicVariable errorInCall,std::map<std::type_index,torch::jit::SymbolicVariable> StackInCall)
            {
                torch::jit::WithInsertPoint guard(body);
                body->registerOutput(errorInCall);
                std::for_each(StackInCall.begin(),StackInCall.end(),[&body,this](auto elem)
                {
                    body->registerOutput(elem.second);
                });
            };
            
            if(i+1<m_nbFunc)
            {
                auto valConstant=graph->insertConstant(at::ones({},at::kCUDA)*int(i));
                auto condIsCall=output_select.create(torch::jit::aten::eq, {output_select,valConstant})[0];
        
                auto nodeIfCall=graph->insertNode(graph->create(torch::jit::prim::If,numStack));
                nodeIfCall->addInput(condIsCall);
                auto nodeCall=nodeIfCall->addBlock();
                ApplyCall(nodeCall,errorInCall,StackInCall);
                auto nodeNotCall=nodeIfCall->addBlock();
                ++i;
                torch::jit::WithInsertPoint guard(nodeNotCall);
                auto varNotCall=CallRecursion(i,nodeNotCall,errorInCall,StackInCall);
                std::for_each(varNotCall->outputs().begin(),varNotCall->outputs().end(),[&nodeNotCall](auto elem)
                {
                    nodeNotCall->registerOutput(elem);
                });
                return nodeIfCall;
            }
            else
            {
                auto valConstant=graph->insertConstant(at::ones({},at::kCUDA)*int(i));
                auto condIsCall=output_select.create(torch::jit::aten::eq, {output_select,valConstant})[0];
        
                auto nodeIfCall=graph->insertNode(graph->create(torch::jit::prim::If,numStack));
                nodeIfCall->addInput(condIsCall);
                auto nodeCall=nodeIfCall->addBlock();
                ApplyCall(nodeCall,errorInCall,StackInCall);
                auto nodeNotCall=nodeIfCall->addBlock();
                ++i;
                torch::jit::WithInsertPoint guard(nodeNotCall);
                ApplyIdentity(nodeNotCall,errorInCall,StackInCall);
                return nodeIfCall;
            }
        };
        auto ResultCall=CallRecursion(0,body,error,InputStackIn);
        
        auto cond=Size>torch::jit::SymbolicVariable(counter);
            
        body->registerOutput(cond);
        std::for_each(ResultCall->outputs().begin(),ResultCall->outputs().end(),[&body](auto elem)
        {
            body->registerOutput(elem);
        });
    }
    graph->registerOutput(loop->outputs()[0]);
    auto graphBatch=torch::jit::to_batch_graph(graph);
    graphBatch->lint();
    auto graphFinal=std::make_shared<torch::jit::Graph>();
        
    for(int i=0;i<graphBatch->inputs().size();++i)
    {
        graphFinal->addInput(graphBatch->inputs()[i]->uniqueName());
    }
    auto results=torch::jit::inlineCallTo(*graphFinal,*graphBatch,graphFinal->inputs(),true);
    auto resultData=results[0];
    auto resultMask=results[1];
    auto resultDim=results[2];
    auto tempResult=static_cast<torch::jit::SymbolicVariable>(resultData)*static_cast<torch::jit::SymbolicVariable>(resultMask).type_as(resultData);
    auto oneConstantFinal=graphFinal->insertConstant(0);
    auto size_Node=graphFinal->insertNode(graphFinal->create(torch::jit::aten::size,1));
    size_Node->addInput(tempResult);
    size_Node->addInput(oneConstantFinal);
    auto final_Size=torch::jit::SymbolicVariable(size_Node->outputs()[0]);
    auto finalResult=tempResult.sum();
    auto finalRescale=finalResult/final_Size;
    torch::jit::SymbolicVariable(finalResult).addAsOutput();
    final_Size.addAsOutput();
    m_executor[typeLoss.first]=torch::jit::GraphExecutor(graphFinal,true);
    });
}

template <typename TypeExpressionGenerator>
template<typename Type>
auto ComputationGraphGenerator<TypeExpressionGenerator>::GenerateStack(size_t batchSize,Type_Generator& G)
{
    torch::jit::Stack stack;
    typedef decltype(m_generator.template GetRandom<Type>(G)(G,0)) TypeExpression;
    
    std::deque<TypeExpression> dequeExpressions;
    auto StaticDim=at::ones({1},at::kByte);
    StaticDim[0]=false;
    
    auto NoDimData=at::zeros({batchSize},at::kInt);
    auto NoDimMask=at::ones({batchSize},at::kByte);
    auto NoDimDim=at::zeros({},at::kByte);
    
    auto DynamicDim=at::ones({2},at::kByte);
    DynamicDim[0]=true;
    DynamicDim[1]=false;
    
    auto DynamicDim0=at::ones({1},at::kByte);
    DynamicDim[0]=true;
    
    auto funcVar=[](auto elem)
    {
        return torch::autograd::make_variable(elem.to(at::Device("cuda")),false);
    };
    
    std::vector<at::Tensor> Call;
    Call.reserve(batchSize);
    
    std::map<std::type_index,std::vector<at::Tensor>> ListGen;
    
    std::map<std::type_index,std::vector<at::Tensor>> ListTarget;
    
    std::vector<at::Tensor> Target;
    Target.reserve(batchSize);
    
    for(size_t i=0;i<batchSize;++i)
    {
        while(true)
        {
            auto funcExpressions=m_generator.template GetRandom<Type>(G);
            auto expressions=funcExpressions(G,0);
            size_t maxStack=expressions.back().m_maxStack;
            if(maxStack<=m_stackSize && !(expressions.size()==1 && (m_TypeGen.count(expressions[0].m_output.Get().type())>0)))
            {
            dequeExpressions.push_back(expressions);
            DoExpressionsDebug(expressions);
            NoDimData[i]=static_cast<int>(expressions.size()-1);
        
            at::Tensor callTen=at::zeros({1,expressions.size()},at::kInt);
            auto AccessorCallTen=callTen.packed_accessor<int,2>();
            int j=0;
            std::for_each(expressions.begin(),expressions.end(),[&j,&AccessorCallTen](auto elem)
            {
                AccessorCallTen[0][j]=static_cast<int>(elem.m_func);
                ++j;
            });
            Call.push_back(callTen);
            std::for_each(m_TypeGen.begin(),m_TypeGen.end(),[&expressions,this,&ListGen](auto elem)
            {
                at::Tensor GenTen=at::zeros({1,expressions.size(),m_TypeSize[elem]},at::kFloat);
                auto AccessorGenTen=GenTen.packed_accessor<float,3>();
                int j=0;
                std::for_each(expressions.begin(),expressions.end(),[&j,&AccessorGenTen,this,&elem](auto elem2)
                {
                    if(elem==elem2.m_output.Get().type())
                    {
                        auto temp=AccessorGenTen[0][j];
                        m_anyToTensor[elem](elem2.m_output.Get(),temp);
                    }
                    ++j;
                });
                ListGen[elem].push_back(GenTen);
            }
            );
            std::for_each(m_lossMap.begin(),m_lossMap.end(),[&expressions,this,&ListTarget](auto elem)
            {
                at::Tensor TargetTen=at::zeros({1,expressions.size(),m_TypeSize[elem.first]},at::kFloat);
                auto AccessorTargetTen=TargetTen.packed_accessor<float,3>();
                int j=0;
                std::for_each(expressions.begin(),expressions.end(),[&j,&AccessorTargetTen,this,&elem](auto elem2)
                {
                    if(elem.first==elem2.m_output.Get().type())
                    {
                        auto temp=AccessorTargetTen[0][j];
                        m_anyToTensor[elem.first](elem2.m_output.Get(),temp);
                    }
                    ++j;
                });
                ListTarget[elem.first].push_back(TargetTen);
            }
            );
            break;
            }
        }
    }
    
    stack.push_back(funcVar(NoDimData));
    stack.push_back(funcVar(NoDimMask));
    stack.push_back(funcVar(NoDimDim));
    
    torch::jit::BatchTensor BatchCall(Call,DynamicDim0);
    
    stack.push_back(funcVar(BatchCall.get_data()));
    stack.push_back(funcVar(BatchCall.get_mask()));
    stack.push_back(funcVar(BatchCall.get_dims()));
    
    std::for_each(m_TypeGen.begin(),m_TypeGen.end(),[&ListGen,this,&stack,&funcVar,&DynamicDim](auto elem)
    {
        torch::jit::BatchTensor BatchGen(ListGen[elem],DynamicDim);
        
        stack.push_back(funcVar(BatchGen.get_data()));
        stack.push_back(funcVar(BatchGen.get_mask()));
        stack.push_back(funcVar(BatchGen.get_dims()));
    });
    
    std::for_each(m_lossMap.begin(),m_lossMap.end(),[&ListTarget,this,&stack,&funcVar,&DynamicDim](auto elem)
    {
        torch::jit::BatchTensor BatchTarget(ListTarget[elem.first],DynamicDim);
        
        stack.push_back(funcVar(BatchTarget.get_data()));
        stack.push_back(funcVar(BatchTarget.get_mask()));
        stack.push_back(funcVar(BatchTarget.get_dims()));
    });
    
    torch::jit::BatchTensor BatchError(at::zeros({},at::kCUDA),batchSize);
    stack.push_back(funcVar(BatchError.get_data()));
    stack.push_back(funcVar(BatchError.get_mask()));
    stack.push_back(funcVar(BatchError.get_dims()));
    
    std::for_each(m_TypeReturn.begin(),m_TypeReturn.end(),[&stack,&StaticDim,this,&funcVar,&batchSize](auto elem)
    {
        auto Input=at::zeros({m_stackSize,m_TypeSize[elem]},at::kCUDA);
        torch::jit::BatchTensor BatchInput(Input,batchSize);
        stack.push_back(funcVar(BatchInput.get_data()));
        stack.push_back(funcVar(BatchInput.get_mask()));
        stack.push_back(funcVar(BatchInput.get_dims()));
    });
    
    std::for_each(m_functionMap.begin(),m_functionMap.end(),[&stack,&StaticDim,&batchSize,&funcVar](auto elem)
    {
        for(auto elem2:elem.second.GetParamRef())
        {
            torch::jit::BatchTensor BatchParam(elem2.toTensor(),batchSize);
            stack.push_back(BatchParam.get_data());
            stack.push_back(BatchParam.get_mask());
            stack.push_back(BatchParam.get_dims());
        }
    });
    return std::make_pair(stack,dequeExpressions);
}

template <typename TypeExpressionGenerator>
template<typename Type>
torch::autograd::Variable ComputationGraphGenerator<TypeExpressionGenerator>::run(torch::jit::Stack& stack)
{
    m_executor[std::type_index(typeid(Type))].run(stack);
    return torch::autograd::Variable(stack[0].toTensor())/stack[1].toInt();
}

template <typename TypeExpressionGenerator>
template <typename Expressions>
void ComputationGraphGenerator<TypeExpressionGenerator>::DoExpressionsDebug(Expressions expr)
{
    if(!m_logExpressions)
    {
        return;
    }
    size_t number=m_number.fetch_add(1,std::memory_order_relaxed);
    std::fstream of(std::string("/Data/Debug/graph/")+std::to_string(number),std::fstream::out);
    auto graph=ExpressionsToGraph(expr);
    boost::dynamic_properties dp;
    dp.property("Function", boost::get(&ExpressionsVertexProperties::functionId, graph));
    dp.property("Position", boost::get(&ExpressionsVertexProperties::position, graph));
    dp.property("Stack", boost::get(&ExpressionsVertexProperties::stack, graph));
    dp.property("MaxStack", boost::get(&ExpressionsVertexProperties::maxStack, graph));
    boost::write_graphml(of, graph, dp, false);
}

