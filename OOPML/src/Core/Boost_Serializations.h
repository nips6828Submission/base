#pragma once
#include <boost/serialization/access.hpp>
#include <boost/preprocessor/cat.hpp>
#define BOOST_SERIALIZATIONS friend class boost::serialization::access;template<class Archive> \
    void serialize(Archive & ar, const unsigned int version);
