#pragma once

/*!
 *  @brief Compute mean and variance in an online manner.
 *
 * @tparam ValueType Type of value to use.
*/
template <class ValueType>
class OnlineMeanAndVariance
{
    int m_n=0;
    ValueType m_mean;
    ValueType m_M2;
    ValueType m_Variance;
public:
    /// Default constructor.
    OnlineMeanAndVariance();
    /// Construct with a given mean, variance and number of update done.
    /// @param mean Accumulated mean value.
    /// @param variance Accumulated variance.
    /// @param n Number of iteration.
    OnlineMeanAndVariance(ValueType mean, ValueType variance,int n);
    /// Update mean, variance with new value.
    /// @param val Value to add.
    void Add(ValueType val);
    /// Retrieve mean.
    const ValueType& GetMean() const;
    /// Retrieve Variance.
    const ValueType& GetVariance() const;
    int GetN() const;
};

#include "OnlineMeanAndVariance.tpp"
