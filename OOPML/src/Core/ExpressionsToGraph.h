#pragma once
#include <iostream>
#include <vector>
#include <utility>
#include <string>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/property_map/property_map.hpp>

struct ExpressionsEdgeProperties 
{
};

struct ExpressionsVertexProperties
{
    ExpressionsVertexProperties()
    {
    }
    ExpressionsVertexProperties(size_t f,size_t position_,size_t stack_,size_t maxStack_) : functionId(f), position(position_),stack(stack_),maxStack(maxStack_)
    {
    }
    size_t functionId;
    size_t position;
    size_t stack;
    size_t maxStack;
};

/**
 * @brief Function that create a graph from a given expressions.
 * @tparam Expressions type of the expression.
 * @arg expression Expression to create the graph from.
 * @return The graph created.
 */
template <class Expressions>
auto ExpressionsToGraph(Expressions expression)
{
    typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, ExpressionsVertexProperties, ExpressionsEdgeProperties> Graph;
    Graph g;
    ExpressionsEdgeProperties edge;
    typedef boost::graph_traits<Graph>::vertex_descriptor vertex_descriptor;
    std::map<void*,vertex_descriptor> vertex_map;
    size_t pos=0;
    std::for_each(expression.begin(),expression.end(),[&](auto elem)
    {
        auto vertex=boost::add_vertex(ExpressionsVertexProperties(elem.m_func,pos,elem.m_stack,elem.m_maxStack),g);
        vertex_map[static_cast<void*>(elem.m_output.GetPointer().get())]=vertex;
        std::for_each(elem.m_inputs.begin(),elem.m_inputs.end(),[&](auto elem2)
        {
            boost::add_edge(vertex_map[static_cast<void*>(elem2.GetPointer().get())],vertex_map[static_cast<void*>(elem.m_output.GetPointer().get())],edge,g);
        });
        ++pos;
    });
    return g;
}
