#pragma once
#include <type_traits>

/**
 * @brief Done class storing a boolean indicating its state.
 * It has method to 
 */
class Done
{
    bool m_done=false;
public:
    /**
     * @brief Return the state of the boolean.
     * @return State of the boolean.
     */
    virtual bool AlreadyDone() const
    {
        return m_done;
    };
    /**
     * @brief Return the state of the boolean.
     * @return State of the boolean.
     */
    virtual bool AlreadyDone()
    {
        return m_done;
    };
    /**
     * @brief Set the state of the boolean to done.
     */
    virtual void SetDone()
    {
        m_done=true;
    };
    /**
     * @brief Conversion to boolean, equivalent to already done.
     */
    operator bool() const
    {
        return AlreadyDone();
    };
};

/**
 * @brief Concept indicating existence of a function AlreadyDone and SetDone.
 */
template<typename T>
concept bool hasDone = std::is_convertible<T,Done>::value;
