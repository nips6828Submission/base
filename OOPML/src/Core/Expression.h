#pragma once

#include <deque>
#include "Symbolic_Variable.h"
#include "FunctionOp.h"
#include <iostream>
#include <any>
/**
 * @brief Expression represent an element of a functional expression ie a single function call.
 */
struct Expression
{
    /**
     * @brief Lists of inputs.
     */
    std::deque<Symbolic_Variable> m_inputs;
    /**
     * @brief The output.
     */
    Symbolic_Variable m_output;
    /**
     * @brief Id of the function.
     */
    size_t m_size;
    size_t m_func;
    size_t m_stack=0;
    size_t m_maxStack=0;
    
    /**
     * @brief Function to stream.
     */
    friend std::ostream& operator<<(std::ostream& os, const Expression& expr);
};

/**
 * @brief Output stream operator.
 * @arg os Stream to use.
 * @arg expr Expression to dump.
 * @return stream with the dump.
 */
std::ostream& operator<<(std::ostream& os, const Expression& expr)
{
    os<<"Inputs:"<<std::endl;
    std::for_each(expr.m_inputs.begin(),expr.m_inputs.end(),[&os](auto elem)
    {
        os<<elem<<std::endl;
    });
    os<<"Output: "<<expr.m_output<<std::endl;
    os<<"Function: "<<expr.m_func<<std::endl;
    return os;
}

BOOST_FUSION_ADAPT_STRUCT(Expression,m_inputs,m_output,m_size,m_func)
