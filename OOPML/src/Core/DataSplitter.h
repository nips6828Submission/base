#pragma once

#include <string>
#include <stdexcept>
#include "Data.h"
#include <algorithm>

/**
 * @brief Class to split the data into training, testing and validation.
 */
template <typename TypeData>
class DataSplitter
{
    /**
     * @brief Fraction of data used for test
     */
    double m_fractionTest;
    /**
     * @brief Fraction of data used for Validation
     */
    double m_fractionVal;
    /**
     * @brief Fraction of data not available for training.
     */
    double m_fractionSum;
public:
    /**
     * @brief Constructor for DataSplitter.
     */
    DataSplitter(double fractionTest, double fractionVal):m_fractionTest(fractionTest),m_fractionVal(fractionVal)
    {
        if(fractionTest>=1)
        {
            throw std::runtime_error(std::string("Invalid option fractionTest>=1. Used value of fractionTest: ")+std::to_string(fractionTest));
        }
        if(fractionVal>=1)
        {
            throw std::runtime_error(std::string("Invalid option fractionVal>=1. Used value of fractionVal: ")+std::to_string(fractionVal));
        }
        m_fractionSum=fractionVal+fractionTest;
        if(m_fractionSum>=1)
        {
            std::runtime_error(std::string("Invalid option fractionVal+fractionTest>=1. Used value of fractionTest: ")+std::to_string(fractionTest)+std::string(" fractionVal: ")+std::to_string(fractionVal));
        }
    };
    double GetFractionTest()
    {
        return m_fractionTest;
    };
    Data<TypeData>& Split(const TypeData& data,Data<TypeData>& D)
    {
        size_t nbData=data.size()*(1-m_fractionSum);
        TypeData& DataTrain=D.TrainData();
        std::for_each(data.begin(),data.begin()+nbData+1,[&DataTrain](auto elem)
        {
            DataTrain.push_back(elem);
        });
    
        size_t nbDataTest=data.size()*m_fractionTest;
    
        TypeData& DataTest=D.TestData();
        std::for_each(data.begin()+nbData+1,data.begin()+nbData+2+nbDataTest,[&DataTest](auto elem)
        {
            DataTest.push_back(elem);
        });
    
        TypeData& DataVal=D.ValData();
        std::for_each(data.begin()+nbData+2+nbDataTest,data.end(),[&DataVal](auto elem)
        {
            DataVal.push_back(elem);
        });
        return D; 
    };
    Data<TypeData> Split(const TypeData& data)
    {
        Data<TypeData> D;
        return Split(data,D);
    };
};
