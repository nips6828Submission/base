#pragma once
#include <string>
#include "torch/csrc/api/include/torch/serialize.h"
#include <optional>
#include <functional>
#include "torch/csrc/jit/graph_executor.h"
#include "torch/csrc/jit/symbolic_variable.h"

template <class Function>
class FunctionMLToNonML
{
public:
    typedef Function Type;
};

template <typename Return,typename ... Args>
class FunctionMLToNonML<std::function<Return(Args...)>>
{
public:
    typedef std::function<typename Return::Type(typename Args::Type...)> Type;
};

class FunctionOp_Impl
{
    /**
     * @brief Computational graph.
     */
    std::shared_ptr<torch::jit::Graph> m_graph;
    
    /**
     * @brief Computational graph.
     */
    std::shared_ptr<torch::jit::Graph> m_noBatchGraph;
    
    /**
     * @brief Stack of parameters that should be used as input for the parameter portion.
     */
    torch::jit::Stack m_params;
    
    /**
     * @brief Graph executor of m_graph.
     */
    torch::jit::GraphExecutor m_executor;
    
    template <typename Func>
    friend class FunctionOp;
    
public:
    FunctionOp_Impl()
    {
    };
    FunctionOp_Impl(const FunctionOp_Impl& Op):m_graph(Op.m_graph),m_noBatchGraph(Op.m_noBatchGraph),m_params(Op.m_params),m_executor(m_noBatchGraph,true)
    {
    };
    
    FunctionOp_Impl(FunctionOp_Impl&& Op):m_graph(Op.m_graph),m_noBatchGraph(Op.m_noBatchGraph),m_params(Op.m_params),m_executor(m_noBatchGraph,true)
    {
    };
    
    FunctionOp_Impl operator=(const FunctionOp_Impl& Op)
    {
        m_graph=Op.m_graph;
        m_params=Op.m_params;
        m_executor=Op.m_executor;
        m_noBatchGraph=Op.m_noBatchGraph;
        return *this;
    };
    
    FunctionOp_Impl operator=(FunctionOp_Impl&& Op)
    {
        m_graph=Op.m_graph;
        m_params=Op.m_params;
        m_executor=Op.m_executor;
        m_noBatchGraph=Op.m_noBatchGraph;
        return *this;
    };
    
    /**
     * @brief Load member function which update the parameters with a version loaded from the archive.
     * @param archive Archive were the parameters are stored.
     */
    void Load(torch::serialize::InputArchive& archive);
    /**
     * @brief Save member function which save the current parameter and return and archive that can be used to save the content to disc.
     */
    torch::serialize::OutputArchive Save() const;
    /**
     * @brief Constructor from a computational graph and parameters.
     * @param graph Computation graph of the function.
     * @param param Stack of parameter to use.
     * 
     * The number of input of the graph should be number of parameters(param) + number of inputs of the function.
     * The graph should expect the inputs as first argument followed by the parameters.
     */
    FunctionOp_Impl(std::shared_ptr<torch::jit::Graph> graph,std::shared_ptr<torch::jit::Graph> graphNoBatch,torch::jit::Stack param);
    std::shared_ptr<torch::jit::Graph> GetGraph() const
    {
        return m_graph;
    };
    /**
     * @brief Get the parameters.
     */
    torch::jit::Stack GetParam() const
    {
        return m_params;
    }
    /**
     * @brief Get a reference to the parameters.
     */
    torch::jit::Stack& GetParamRef()
    {
        return m_params;
    }
    
    torch::jit::GraphExecutor& GetGraphExecutor()
    {
        return m_executor;
    }
    
    /**
     * @brief Add this function operation to the graph.
     * @param graph Where to add the function.
     * @param param to use. 
     */
    std::vector<torch::jit::SymbolicVariable> InsertNode(torch::jit::Graph* graph,torch::jit::ArrayRef<torch::jit::SymbolicVariable> symbParams,torch::jit::ArrayRef<torch::jit::SymbolicVariable> inputs);
    std::vector<torch::jit::SymbolicVariable> InsertNode(std::shared_ptr<torch::jit::Graph> graph,torch::jit::ArrayRef<torch::jit::SymbolicVariable> symbParams,torch::jit::ArrayRef<torch::jit::SymbolicVariable> inputs)
    {
        return InsertNode(graph.get(),symbParams,inputs);
    }
    /**
     * @brief Create needed Symbolic Parameters into the graph.
     */
    std::vector<torch::jit::SymbolicVariable> CreateSymbolicParameters(torch::jit::Graph* graph);
    std::vector<torch::jit::SymbolicVariable> CreateSymbolicParameters(std::shared_ptr<torch::jit::Graph> graph)
    {
        return CreateSymbolicParameters(graph.get());
    }
};

template <typename FunctionType>
class FunctionOp;

template <>
class FunctionOp<void>;

/**
 * @brief This is a container class containing and individual custom function together with functions to obtain its graph and call the function. In addition, it can contain parameters.
 * 
 * @tparam FunctionType the type of function. For the moment only std::function is supported.
 * 
 * We call input the input of the function we will provide. We call parameters additional data stored into this class. This is meant to store weights of a neural network.
 */
template <typename FunctionType>
class FunctionOp
{
public:
    typedef typename FunctionMLToNonML<FunctionType>::Type TypeFunctionNonML;
private:
    FunctionOp_Impl m_impl;
    /**
     * @brief Function allowing to call using only the inputs and not the parameter.
     */
    FunctionType m_func;
    
    std::optional<TypeFunctionNonML> m_funcReal;
    /**
     * @brief Auxiliary function which create a function allowing to apply the graph using only the inputs using the parameter of this class.
     * @param FunctionOp An instance of the function op class.
     * @return A lambda function computing the output from the inputs without parameters.
     */
    template <typename Return,typename... Arg>
    friend std::function<Return(Arg...)> GetLambda(FunctionOp<std::function<Return(Arg...)>>&);

public:
    /**
     * @brief Default constructor. The object created by this construct is not usable and an assignation to an usable version should be done.
     */
    FunctionOp()
    {
    };
    
    typedef FunctionType TypeFunction;
    
    /**
     * @brief Get the function.
     * Warning the obtained function should not be used after the lifetime of this class.
     */
    FunctionType GetFunction() const
    {
        return m_func;
    }
    
    TypeFunctionNonML GetFunctionReal() const
    {
        return m_funcReal.value();
    }
    
    
    FunctionOp<void> GetGeneric() const;
    
    FunctionOp(const FunctionOp& Op):m_impl(Op.m_impl),m_func(GetLambda(*this)),m_funcReal(Op.m_funcReal)
    {
    };
    
    FunctionOp(FunctionOp&& Op):m_impl(Op.m_impl),m_func(GetLambda(*this)),m_funcReal(Op.m_funcReal)
    {
    };
    
    FunctionOp operator=(const FunctionOp& Op)
    {
        m_impl=Op.m_impl;
        m_func=GetLambda(*this);
        m_funcReal=Op.m_funcReal;
        return *this;
    };
    
    FunctionOp operator=(FunctionOp&& Op)
    {
        m_impl=Op.m_impl;
        m_func=GetLambda(*this);
        m_funcReal=Op.m_funcReal;
        return *this;
    };
    
    /**
     * @brief Load member function which update the parameters with a version loaded from the archive.
     * @param archive Archive were the parameters are stored.
     */
    void Load(torch::serialize::InputArchive& archive)
    {
        m_impl.Load(archive);
    }
    /**
     * @brief Save member function which save the current parameter and return and archive that can be used to save the content to disc.
     */
    torch::serialize::OutputArchive Save() const
    {
        return m_impl.Save();
    }
    /**
     * @brief Constructor from a computational graph and parameters.
     * @param graph Computation graph of the function.
     * @param param Stack of parameter to use.
     * 
     * The number of input of the graph should be number of parameters(param) + number of inputs of the function.
     * The graph should expect the inputs as first argument followed by the parameters.
     */
    FunctionOp(std::shared_ptr<torch::jit::Graph> graph,std::shared_ptr<torch::jit::Graph> graphNoBatch,torch::jit::Stack param);
    FunctionOp(std::shared_ptr<torch::jit::Graph> graph,std::shared_ptr<torch::jit::Graph> graphNoBatch,torch::jit::Stack param,const TypeFunctionNonML& funcReal);
    std::shared_ptr<torch::jit::Graph> GetGraph() const
    {
        return m_impl.GetGraph();
    };
    /**
     * @brief Get the parameters.
     */
    torch::jit::Stack GetParam() const
    {
        return m_impl.GetParam();
    }
    /**
     * @brief Get a reference to the parameters.
     */
    torch::jit::Stack& GetParamRef()
    {
        return m_impl.GetParamRef();
    }
    
    /**
     * @brief Add this function operation to the graph.
     * @param graph Where to add the function.
     * @param param to use. 
     */
    std::vector<torch::jit::SymbolicVariable> InsertNode(torch::jit::Graph* graph,torch::jit::ArrayRef<torch::jit::SymbolicVariable> symbParams,torch::jit::ArrayRef<torch::jit::SymbolicVariable> inputs)
    {
        return m_impl.InsertNode(graph,symbParams,inputs);
    }
    std::vector<torch::jit::SymbolicVariable> InsertNode(std::shared_ptr<torch::jit::Graph> graph,torch::jit::ArrayRef<torch::jit::SymbolicVariable> symbParams,torch::jit::ArrayRef<torch::jit::SymbolicVariable> inputs)
    {
        return m_impl.InsertNode(graph,symbParams,inputs);
    }
    /**
     * @brief Create needed Symbolic Parameters into the graph.
     */
    std::vector<torch::jit::SymbolicVariable> CreateSymbolicParameters(torch::jit::Graph* graph)
    {
        return m_impl.CreateSymbolicParameters(graph);
    }
    std::vector<torch::jit::SymbolicVariable> CreateSymbolicParameters(std::shared_ptr<torch::jit::Graph> graph)
    {
        return m_impl.CreateSymbolicParameters(graph);
    }
    
};

/**
 * @brief This is a container class containing and individual custom function together with functions to obtain its graph and call the function. In addition, it can contain parameters.
 * 
 * @tparam FunctionType the type of function. For the moment only std::function is supported.
 * 
 * We call input the input of the function we will provide. We call parameters additional data stored into this class. This is meant to store weights of a neural network.
 */
template <>
class FunctionOp<void>
{
    FunctionOp_Impl m_impl;

public:
    /**
     * @brief Default constructor. The object created by this construct is not usable and an assignation to an usable version should be done.
     */
    FunctionOp()
    {
    };
    
    FunctionOp<void> GetGeneric() const
    {
        return FunctionOp<void>(m_impl);
    };
    
    FunctionOp(const FunctionOp& Op):m_impl(Op.m_impl)
    {
    };
    
    FunctionOp(FunctionOp&& Op):m_impl(Op.m_impl)
    {
    };
    
    FunctionOp operator=(const FunctionOp& Op)
    {
        m_impl=Op.m_impl;
        return *this;
    };
    
    FunctionOp operator=(FunctionOp&& Op)
    {
        m_impl=Op.m_impl;
        return *this;
    };
    
    /**
     * @brief Load member function which update the parameters with a version loaded from the archive.
     * @param archive Archive were the parameters are stored.
     */
    void Load(torch::serialize::InputArchive& archive)
    {
        m_impl.Load(archive);
    }
    /**
     * @brief Save member function which save the current parameter and return and archive that can be used to save the content to disc.
     */
    torch::serialize::OutputArchive Save() const
    {
        return m_impl.Save();
    }
    /**
     * @brief Constructor from a computational graph and parameters.
     * @param graph Computation graph of the function.
     * @param param Stack of parameter to use.
     * 
     * The number of input of the graph should be number of parameters(param) + number of inputs of the function.
     * The graph should expect the inputs as first argument followed by the parameters.
     */
    FunctionOp(std::shared_ptr<torch::jit::Graph> graph,std::shared_ptr<torch::jit::Graph> graphNoBatch,torch::jit::Stack param);
    FunctionOp(const FunctionOp_Impl& impl):m_impl(impl)
    {
    };
    
    std::shared_ptr<torch::jit::Graph> GetGraph() const
    {
        return m_impl.GetGraph();
    };
    /**
     * @brief Get the parameters.
     */
    torch::jit::Stack GetParam() const
    {
        return m_impl.GetParam();
    }
    /**
     * @brief Get a reference to the parameters.
     */
    torch::jit::Stack& GetParamRef()
    {
        return m_impl.GetParamRef();
    }
    
    /**
     * @brief Add this function operation to the graph.
     * @param graph Where to add the function.
     * @param param to use. 
     */
    std::vector<torch::jit::SymbolicVariable> InsertNode(torch::jit::Graph* graph,torch::jit::ArrayRef<torch::jit::SymbolicVariable> symbParams,torch::jit::ArrayRef<torch::jit::SymbolicVariable> inputs)
    {
        return m_impl.InsertNode(graph,symbParams,inputs);
    }
    std::vector<torch::jit::SymbolicVariable> InsertNode(std::shared_ptr<torch::jit::Graph> graph,torch::jit::ArrayRef<torch::jit::SymbolicVariable> symbParams,torch::jit::ArrayRef<torch::jit::SymbolicVariable> inputs)
    {
        return m_impl.InsertNode(graph,symbParams,inputs);
    }
    /**
     * @brief Create needed Symbolic Parameters into the graph.
     */
    std::vector<torch::jit::SymbolicVariable> CreateSymbolicParameters(torch::jit::Graph* graph)
    {
        return m_impl.CreateSymbolicParameters(graph);
    }
    std::vector<torch::jit::SymbolicVariable> CreateSymbolicParameters(std::shared_ptr<torch::jit::Graph> graph)
    {
        return m_impl.CreateSymbolicParameters(graph);
    }
    
};

#include "Core/FunctionOp.tpp"
