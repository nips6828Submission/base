#pragma once
#include "Concepts/TypeData.h"
#include "Done.h"
#include "Data.h"
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

/**
 * @brief Generic data container class.
 * @tparam DataType Type of the data to store.
 */
template <CTypeData DataType>
class ExperimentData : public Data<DataType>, public Done
{
    /**
     * @brief Name of the data.
     */
    std::string m_dataName;
public:
    /**
     * @brief Constructor from a name.
     */
    ExperimentData(std::string dataName): m_dataName(dataName)
    {
    }
    /**
     * @brief Get path to the directory.
     * @return Path string.
     */
    virtual std::string GetDirectoryPath() const=0;
    /**
     * @brief Get name of the Data.
     * @return Name of the data.
     */
    virtual std::string GetDataName() const
    {
        return m_dataName;
    }
    /**
     * @brief Print a description of the data.
     */
    virtual void PrintDescription() const
    {
        std::cout<<"nameData: "<<GetDataName()<<std::endl;
        Data<DataType>::PrintDescription();
    }
    /**
     * @brief Return if data preparation is already done.
     */
    virtual bool AlreadyDone()
    {
        if(Done::AlreadyDone())
        {
            return true;
        }
        else
        {
            if(boost::filesystem::exists(boost::filesystem::path{GetDirectoryPath()+std::string("/done")}))
            {
                Done::SetDone();
                return true;
            }
        }
        return false;
    };
    /**
     * @brief Set as done.
     */
    virtual void SetDone()
    {
        boost::filesystem::create_directories(boost::filesystem::path{GetDirectoryPath()});
        std::ofstream oftouch(GetDirectoryPath()+std::string("/done"));
        oftouch<<"done"<<std::endl;
    };
    /**
     * @brief Save the data.
     */
    virtual void Save() const
    {
        std::ofstream ofd(GetDirectoryPath()+std::string("/Data.dat"));
        boost::archive::binary_oarchive ard(ofd);
        ard<<*this;
    };
    /**
     * @brief Load the data.
     */
    virtual void Load()
    {
        std::ifstream ofd(GetDirectoryPath()+std::string("/Data.dat"));
        boost::archive::binary_iarchive ard(ofd);
        ard>>*this;
    };
    /**
     * @brief The data.
     */
    typedef DataType type_data;
};

#include "Concepts/ExperimentData.h"
