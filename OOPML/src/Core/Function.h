#pragma once
#include <functional>

/**
 * @brief Type trait to compute number of argument of a std::function.
 */
template <typename Class>
struct SizeArgument
{
};

template <typename Return,typename... Arg>
struct SizeArgument<std::function<Return(Arg...)>>
{
    /**
    * @brief Number of argument of the function.
    */
    static const size_t size=sizeof...(Arg);
};

/**
* @brief Type trait to compute the type of argument np of a function.
* @tparam Func function to observe.
* @tparam nb Which argument to consider.
*/
template <typename Func,int nb>
struct TypeArgument
{
};

template <typename Return,typename... Arg,int nb>
struct TypeArgument<std::function<Return(Arg...)>,nb>
{
    /**
    * Type of the argument np of the function.
    */
    typedef typename std::tuple_element<nb,std::tuple<Arg...>>::type type;
};

/**
* @brief Implementation class used to implement TupleTransform
*/
template <typename Tuple,typename F,size_t ... indices>
auto TupleTransform(Tuple t,F f,std::index_sequence<indices...>)
{
    return std::make_tuple(f(std::get<indices>(t))...);
}

/**
* @brief Apply a single variable function on every element of a tuple.
* @tparam Tuple type of tuple to consider.
* @tparam F type of function to consider.
* @param t tuple to consider.
* @param f function to consider.
*/
template <typename Tuple,typename F>
auto TupleTransform(Tuple t,F f)
{
    return TupleTransform<Tuple,F>(t,f,std::make_index_sequence<std::tuple_size<Tuple>::value>());
}

#include "Core/Pytorch.h"

template <typename F,typename... Arg >
auto TryUntilSucess(const F& f,Arg&&... arg)
{
    while(true)
    {
        try
        {
            return f(std::forward<Arg>(arg)...);
        }
        catch(boost::python::error_already_set const &)
        {
            PyErr_Print();
        }
        catch(const std::exception& e)
        {
        }
        catch(...)
        {
        }
    }
}
