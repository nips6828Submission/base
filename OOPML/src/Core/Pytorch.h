#pragma once
#include <boost/python.hpp>
#include <boost/python/numpy.hpp>
#include <iostream>

/**
 * @brief Class initializing pytorch and containing handle to pytorch object.
 */
class Pytorch
{
public:
    /**
     * @brief Constructor, initializing python. It also initialize PyTorch together than a hook so that everything work correctly.
     */
    Pytorch();
};

#include "Pytorch.tpp"
