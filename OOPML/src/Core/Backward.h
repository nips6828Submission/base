#pragma once
#include <torch/csrc/autograd/python_variable.h>

/**
 * @brief Implementation of Backpropagation.
 * @arg tensor Variable to backpropagate from.
 * @arg grad Initial gradient to use for backpropagation (normally should be 1).
 * 
 * Once this method called the grad field of all leaf variable will be updated.
 */
void Backward(torch::autograd::Variable& tensor,torch::autograd::Variable& grad);

#include "Backward.tpp"
