#pragma once
#include <map>
#include "torch/csrc/jit/symbolic_variable.h"
#include "Core/FunctionOp.h"
#include "torch/csrc/jit/batched/BatchTensor.h"
#include "torch/csrc/jit/passes/to_batch.h"
#include <memory>
#include <functional>
#include <any>
#include <atomic>

/**
 * @brief Computational Graph generator. This class generate a computation graph and apply it with a given functional expression.
 * 
 * @todo This class is WIP.
 * 
 * @tparam TypeExpressionGenerator Class to use for expression generator.
 * 
 * This class usage is the following:
 * - Create an instance of this class.
 * - Add function and generator function with InsertGenerator and Insert.
 * - Call CreateGraphExecutor which will create the executor, this function need to be called after all addition of new functions.
 * - Generate a stack with GenerateStack<double>(2,generator)
 * - Run the graph with Graph.run(stack).
 */
template <typename TypeExpressionGenerator>
class ComputationGraphGenerator
{
    /**
     * @brief Indicate if we should log the graph of expressions.
     */
    bool m_logExpressions=false;
    /**
     * @brief Internal number indicating howe many debug expression where output.
     */
    std::atomic<size_t> m_number=0;
    /**
     * @brief Generator of expression.
     *
     * This object will be used to generate all expression.
     */
    TypeExpressionGenerator m_generator;
    /**
     * @brief Typedef of the Type generator.
     */
    typedef typename TypeExpressionGenerator::type_Generator Type_Generator;
    /**
     * @brief Map of functions.
     * 
     * This is used to map id to the function and computational graph of the function.
     */
    std::map<size_t,FunctionOp<void>> m_functionMap;
    /**
     * @brief Mapping of type to loss functions.
     * 
     * This is used to map id to the loss and computational graph of the loss.
     */
    std::map<std::type_index,FunctionOp<void>> m_lossMap;
    /**
     * @brief A list of SymbolicVariable which are ordered.
     * 
     * This is used to represent the input of a function.
     */
    typedef std::vector<torch::jit::SymbolicVariable> Type_InputStack;
    /**
     * @brief Typedef to the function which compute the vector of input symbolic variable using a stack as input together with the stack after having consumed the input.
     */
    
    /**
     * @brief This represent a map of stacks where the stack is represented by a single Tensor (SymbolicVariable). And each stack is for a different type.
     */
    typedef std::map<std::type_index,torch::jit::SymbolicVariable> Type_FullStack;
    
    /**
     * @brief Function which from a Full stack return an updated Full stack together with an InputStack.
     * 
     * This function is used to define functions by there stack manipulation. The InputStack is the actual input of the stack whereas the FullStack is the state of the Stack after consumption of the input.
     */
    typedef std::function<std::pair<Type_InputStack,Type_FullStack>(Type_FullStack)> Type_Function;
    /**
     * @brief Map from function index to the Type function.
     * 
     * This is used to define the stack manipulation of all the functions.
     */
    std::map<size_t,Type_Function> m_functionStackUsage;
    /**
     * @brief Map containing the size of each type.
     */
    std::map<std::type_index,size_t> m_TypeSize;
    /**
     * @brief Set of all return type.
     */
    std::set<std::type_index> m_TypeReturn;
    /**
     * @brief Set of all Generation type.
     */
    std::set<std::type_index> m_TypeGen;
    /**
     * @brief Map of the generator function to the return type.
     */
    std::map<size_t,std::type_index> m_GeneratorFunc;
    
    /**
     * @brief Map of the function to the return type.
     */
    std::map<size_t,std::type_index> m_ReturnFunc;
    /**
     * @brief Map to the function that transforms std::any to tensor.
     * 
     * This is needed because to be able to go from std::any to tensor, we need first to go from any to the underlying type.
     * However this transformation to the underlying type can only be written by the code that registered the type. Because we can only store a type by std::type_index.
     * 
     */
    std::map<std::type_index,std::function<void(std::any,at::TensorAccessor<float, 1>&)>> m_anyToTensor;
    /**
     * @brief Size of the stack per type.
     * 
     * The stack is the main structure used to remember temporary computation.
     * Using a stack which is too small will result in a silence error, because of truncation of stack.
     */
    size_t m_stackSize=3;
    /**
     * @brief Map from type to executor.
     * 
     * This is used to get the executor that execute the computational graph for every "loss" type.
     */
    std::map<std::type_index,torch::jit::GraphExecutor> m_executor;
    /**
     * @brief Number of function.
     */
    size_t m_nbFunc=0;
    
    /**
     * @brief Vector of all learnable parameter.
     */
    std::vector<at::Tensor> m_paramTensor;
    
    /**
     * @brief Function which update TypeReturn and TypeSize as needed
     * @tparam Return Type of the return.
     * @tparam Args Type of the arguments.
     * @param func Function use for update.
     */
    template<typename Return,class ...Args>
    void UpdateType(const std::function<Return(Args...)>& func);
    /**
     * @brief Function which return the function doing the needed stack manipulation to get the input and return the stack with the input consumed.
     * @tparam Return Type of the return.
     * @tparam Args Type of the arguments.
     * @param func Function use for update.
     * @return The function return the stack usage.
     */
    template<typename Return,class ...Args>
    Type_Function GetStackUsage(const std::function<Return(Args...)>& func);
    /**
     * @brief Function which return the Return type index.
     * @tparam Return Type of the return.
     * @tparam Args Type of the arguments.
     * @param func Function use for update.
     * @return The index of the return value.
     */
    template<typename Return,class ...Args>
    std::type_index GetReturnIndex(const std::function<Return(Args...)>& func);
    
    /**
     * @brief Function which return the type of the loss.
     * @tparam Return Type of the return.
     * @tparam Arg Type of the arguments.
     * @param func Function use for update.
     * @return The index of the return value.
     */
    template<typename Return,typename Arg>
    std::type_index GetReturnLossIndex(const std::function<Return(Arg,Arg)>& func);
    
    /**
     * @brief Function which update the count of number of item of a type and map the type to the current position. This is an auxiliary function for GetStackUsage.
     * @tparam Arg Type to use for update.
     */
    template<typename Arg>
    void CountUsage(std::vector<std::pair<std::type_index,size_t>>&,std::map<std::type_index,size_t>&);
    
    /**
     * @brief Save the expressions graph on disk if its activated.
     * @tparam Expressions type of the expression.
     * @arg expr Expressions to save.
     * 
     * This function will save a file on /Data/Debug/graph/id where id is an increasing number.
     * The graph is saved with the graphml format.
     */
    template <typename Expressions>
    void DoExpressionsDebug(Expressions expr);
    
public:
    
    /**
     * @brief Set that we want to save the graph of every expressions.
     */
    void SetLogExpressions()
    {
        m_logExpressions=true;
    }
    /**
     * @brief Constructor from a TypeExpressionGenerator.
     * @param Generator Generator to use.
     */
    ComputationGraphGenerator(const TypeExpressionGenerator& Generator):m_generator(Generator.m_generator),m_functionMap(Generator.m_functionMap),m_stackSize(Generator.m_stackSize)
    {
    };
    /**
     * @brief Constructor from a TypeExpressionGenerator.
     * @param Generator Generator to use.
     */
    ComputationGraphGenerator(TypeExpressionGenerator&& Generator):m_generator(Generator.m_generator),m_functionMap(Generator.m_functionMap),m_stackSize(Generator.m_stackSize)
    {
    };
    ComputationGraphGenerator(size_t stackSize=3);
    /**
     * @brief Insert a function to the generator.
     */
    template <class FunctionOp>
    void Insert(const FunctionOp& f,bool depth=false);
    
    /**
     * @brief Insert a function to the generator.
     */
    template <class FunctionOp,class FunctionGen>
    void Insert(const FunctionOp& f,const FunctionGen& g,bool depth=false);
    
    /**
     * @brief Insert a loss function.
     */
    template <class FunctionOp>
    void InsertLoss(const FunctionOp& f);
    
    /**
     * @brief Insert a function generator to the generator.
     */
    template<typename Result>
    void InsertGenerator(std::function<Result(Type_Generator&,size_t)> f,bool depth=false);
    
    /**
     * @brief Create the Graph executor, it need to be called when all function where added.
     */
    void CreateGraphExecutor();
    
    /**
     * @brief Execute the stack.
     * @tparam Type for which execute the stack.
     */
    template<typename Type>
    torch::autograd::Variable run(torch::jit::Stack& stack);
    
    /**
     * @brief Function to generate the stack.
     * @tparam Type to start the generation backward from.
     * @param batchSize Size of the batch.
     * @param Type_Generator
     */
    template<typename Type>
    auto GenerateStack(size_t batchSize,Type_Generator& G);
    
    /**
     * @brief Return all learnable parameter.
     */
    std::vector<at::Tensor>& GetParam()
    {
        return m_paramTensor;
    };
    
    /**
     * @brief Return a pointer to the Generator.
     */
    TypeExpressionGenerator& GetGenerator()
    {
        return m_generator;
    }
};

#include "ComputationGraphGenerator.tpp"
