#pragma once
#include "Pytorch.h"
#include <ATen/ATen.h>
#include <torch/csrc/autograd/variable.h>
#include <torch/csrc/jit/ir.h>

/**
 * @brief Class containing the vector of an ML object.
 * @tparam Class Type of the non ML object.
 * @tparam Enable used in specializations.
 */
template <typename Class,typename Enable=void>
class ML: public at::Tensor
{
    public:
    /**
     * @brief size of the embedding.
     */
    static size_t size();
    static constexpr bool IsFloatCompatible();
    ML();
    ML(at::Tensor embedded);
    typedef Class Type;
};

template <typename FloatType>
class ML<FloatType,typename std::enable_if_t<std::is_floating_point<FloatType>::value>>: public at::Tensor
{
    public:
    static size_t size();
    ML();
    ML(at::Tensor embedded);
    explicit operator FloatType const();
    static constexpr bool IsFloatCompatible();
    typedef FloatType Type;
};

template <typename IntegerType>
class ML<IntegerType,typename std::enable_if_t<std::is_integral<IntegerType>::value>>: public at::Tensor
{
    public:
    static size_t size();
    ML();
    ML(at::Tensor embedded);
    explicit operator IntegerType const();
    static constexpr bool IsFloatCompatible();
    typedef IntegerType Type;
};

#include "ML.tpp"
