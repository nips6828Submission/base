#pragma once
#include <type_traits>
#include "Core/ML.h"

/**
 * @brief Function to transform an arithmetic value to a double.
 * @tparam Return Type to treat.
 * @tparam Enable Test if the Type respect the condition.
 * @return An ML version.
 */
template <typename Return,typename Type,typename std::enable_if<std::is_arithmetic<Return>::value,int>::type=0>
auto GenerateMLValue(Return res,Type& type)
{
    return ML<Return>(torch::autograd::make_variable(at::ones({1},at::kCUDA)));
}

template <typename Return,typename std::enable_if<std::is_arithmetic<Return>::value,int>::type=0>
auto GenerateMLValue(Return res,at::Tensor tensor)
{
    tensor[0]=res;
    return ML<Return>(tensor);
}

template <typename Return,typename std::enable_if<std::is_arithmetic<Return>::value,int>::type=0,typename AccessorType>
void GenerateMLValue(Return res,at::PackedTensorAccessor<AccessorType, 1> tensor)
{
    tensor[0]=res;
}

template <typename Return,typename std::enable_if<std::is_arithmetic<Return>::value,int>::type=0,typename AccessorType>
void GenerateMLValue(Return res,at::TensorAccessor<AccessorType, 1> tensor)
{
    tensor[0]=res;
}
