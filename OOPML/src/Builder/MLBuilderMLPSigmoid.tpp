#include "MLBuilderMLPSigmoid.h"

template<typename TypeTensor>
MLBuilderMLPSigmoid<TypeTensor>::MLBuilderMLPSigmoid(TypeTensor& device,std::vector<size_t> hiddenSize):MLBuilderMLP<TypeTensor>(device,hiddenSize)
{
}

template<typename TypeTensor>
template<typename Return,class ...Args>
auto MLBuilderMLPSigmoid<TypeTensor>::Build(std::function<Return(Args...)> f)
{
    auto CreateStack=[&]()
    {
        torch::jit::Stack stackParam;
        // Check if there is no argument, in this case we simply learn a constant vector.
        if constexpr(sizeof...(Args)==0)
        {
            stackParam.push_back(InitializeBias(ML<Return>::size()));
            return stackParam;
        }
        
        size_t previousSize=(0 + ... + ML<Args>::size());
        //Loop throught all hidden layer.
        for(int i=0;i<m_hiddenSize.size();++i)
        {
            size_t currentSize=m_hiddenSize[i];
            stackParam.push_back(InitializeWeight(currentSize,previousSize));
            stackParam.push_back(InitializeBias(currentSize));
            previousSize=currentSize;
        }
        size_t currentSize=ML<Return>::size();
        stackParam.push_back(InitializeWeight(currentSize,previousSize));
        stackParam.push_back(InitializeBias(currentSize));
        return stackParam;
    };
    
    auto CreateGraph=[&](auto Batch)
    {
        auto graph=std::make_shared<torch::jit::Graph>();    
        // Check if there is no argument, in this case we simply learn a constant vector.
        if constexpr(sizeof...(Args)==0)
        {
            torch::jit::SymbolicVariable result=torch::jit::SymbolicVariable::asNewInput(*graph);
            result.addAsOutput();
            graph->lint();
            return graph;
        }
    
        std::vector<torch::jit::SymbolicVariable> SymbolicInputs=GenerateInputSymbolic<sizeof...(Args)>(graph);
        int dim=1;
        if(!Batch)
        {
            dim=0;
        }
        torch::jit::SymbolicVariable result;
        if constexpr(sizeof...(Args)==1)
        {
            result=SymbolicInputs[0];
        }
        else
        {
            result=torch::jit::SymbolicVariable::cat(SymbolicInputs,dim);
        }
        //Obtain the total size of the argument.
        size_t previousSize=(0 + ... + ML<Args>::size());
        //Loop throught all hidden layer.
        for(int i=0;i<m_hiddenSize.size();++i)
        {
            auto w=torch::jit::SymbolicVariable::asNewInput(*graph);
            auto b=torch::jit::SymbolicVariable::asNewInput(*graph);
            result=b+torch::jit::SymbolicVariable::create(torch::jit::Symbol::aten("mv"),{w,result})[0];
            result=result.sigmoid();
        }
        auto w=torch::jit::SymbolicVariable::asNewInput(*graph);
        auto b=torch::jit::SymbolicVariable::asNewInput(*graph);
        result=b+torch::jit::SymbolicVariable::create(torch::jit::Symbol::aten("mv"),{w,result})[0];
        result=result;
        result=result.sigmoid();
        result.addAsOutput();
        graph->lint();
        return graph;
    };

    auto graph=CreateGraph(true);
    auto graphNoBatch=CreateGraph(false);
    torch::jit::Stack stackParam=CreateStack();
    return FunctionOp<std::function<ML<Return>(ML<Args>...)>>(graph,graphNoBatch,stackParam,f);
}

