#pragma once
#include "MLBuilderMLP.h"

template<typename TypeTensor=decltype(at::CUDA(at::kFloat))> 
class MLBuilderMLPStd : public MLBuilderMLP<TypeTensor>
{
    using MLBuilderMLP<TypeTensor>::m_device;
    torch::autograd::Variable InitializeBias(size_t size)
    {
        return torch::autograd::make_variable(at::ones({static_cast<long int>(size)},m_device),true);
    };
public:
    /**
     * @brief Constructor for MLP.
     * @param device Device on which to do computation.
     * @param hiddenSize Vector indicating the size of every hidden layer. Zero dimensional vector mean linear.
     */
    MLBuilderMLPStd(TypeTensor& device=at::CUDA(at::kFloat),std::vector<size_t> hiddenSize=std::vector<size_t>()):MLBuilderMLP<TypeTensor>(device,hiddenSize)
    {
    };
    /**
     * Copy constructor.
     * @param copy Class to copy.
     */
    MLBuilderMLPStd(const MLBuilderMLPStd& copy)=default;
};
