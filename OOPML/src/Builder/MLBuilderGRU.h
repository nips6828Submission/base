#pragma once
#include "MLBuilder.h"

/**
 * @brief Specialisation to MLify a function with a GRU.
 * 
 * @tparam TypeTensor Type of tensor default on gpu tensor.
 */
template<typename TypeTensor=decltype(at::CUDA(at::kFloat))> 
class MLBuilderGRU : public MLBuilder<TypeTensor>
{
protected:
    using MLBuilder<TypeTensor>::m_device;
    
    /**
     * @brief Zero initialize the bias.
     * @param size of the bias.
     * @return A zero initialized variable.
     */
    torch::autograd::Variable InitializeBias(size_t size);
    
    /**
     * @brief Uniform Initialization of the weight tensor.
     * @param size1 First dimension size.
     * @param size2 Second dimension size.
     * @return The initialized matrix.
     */
    torch::autograd::Variable InitializeWeight(size_t size1,size_t size2);

public:
    /**
     * @brief Constructor for GRU.
     * @param device Device on which to do computation.
     */
    MLBuilderGRU(TypeTensor& device=at::CUDA(at::kFloat));
    /**
     * Copy constructor.
     * @param copy Class to copy.
     */
    MLBuilderGRU(const MLBuilderGRU& copy)=default;


    /**
     * MLify a function.
     * @param f Function to MLify.
     * @return A FunctionOperator which contain the function and parameter.
     */
    template<typename Return,class ...Args>
    auto Build(std::function<Return(Return,Args...)> f);
};

#include "MLBuilderGRU.tpp"

