#include "Builder/MLBuilder.h"
#include <boost/filesystem.hpp>
#include "Core/GenerateMLValue.h"
#include <torch/csrc/autograd/python_variable.h>

template<typename TypeTensor>
MLBuilder<TypeTensor>::MLBuilder(TypeTensor& device):m_device(device)
{
}

template<typename TypeTensor>
MLBuilder<TypeTensor>::MLBuilder(const MLBuilder& copy):m_device(copy.m_device)
{
}

template<typename TypeTensor>
template <class Type>
auto MLBuilder<TypeTensor>::GetML(const Type& val)
{
    return GenerateMLValue(val,m_device);
}

template<typename TypeTensor>
template <class Type,class Tensor>
auto MLBuilder<TypeTensor>::GetML(const Type& val,Tensor ten)
{
    return GenerateMLValue(val,ten);
}
