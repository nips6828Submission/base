#pragma once
#include "MLBuilder.h"

/**
 * @brief Specialisation to MLify a function with a MLP with ReLu activation.
 * 
 * @tparam TypeTensor Type of tensor default on gpu tensor.
 */
template<typename TypeTensor=decltype(at::CUDA(at::kFloat))> 
class MLBuilderMLP : public MLBuilder<TypeTensor>
{
protected:
    using MLBuilder<TypeTensor>::m_device;
    std::vector<size_t> m_hiddenSize;
    
    /**
     * @brief Zero initialize the bias.
     * @param size of the bias.
     * @return A zero initialized variable.
     */
    virtual torch::autograd::Variable InitializeBias(size_t size);
    
    /**
     * @brief Uniform Initialization of the weight tensor.
     * @param size1 First dimension size.
     * @param size2 Second dimension size.
     * @return The initialized matrix.
     */
    virtual torch::autograd::Variable InitializeWeight(size_t size1,size_t size2);

public:
    /**
     * @brief Constructor for MLP.
     * @param device Device on which to do computation.
     * @param hiddenSize Vector indicating the size of every hidden layer. Zero dimensional vector mean linear.
     */
    MLBuilderMLP(TypeTensor& device=at::CUDA(at::kFloat),std::vector<size_t> hiddenSize=std::vector<size_t>());
    /**
     * Copy constructor.
     * @param copy Class to copy.
     */
    MLBuilderMLP(const MLBuilderMLP& copy)=default;


    /**
     * MLify a function.
     * @param f Function to MLify.
     * @return A FunctionOperator which contain the function and parameter.
     */
    template<typename Return,class ...Args>
    auto Build(std::function<Return(Args...)> f);
};

#include "MLBuilderMLP.tpp"
