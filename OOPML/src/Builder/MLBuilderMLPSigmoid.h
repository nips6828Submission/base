#pragma once
#include "MLBuilderMLP.h"

/**
 * @brief This class build an MLP with sigmoid activation everywhere. It is meant to be used with binary representation.
 * 
 * Given \f$x\f$ as input it compute the output next layer as:
 * \f[h=\sigma(6(W_1x+b_1))\f]
 * The final layer is computed by:
 * \f[y=\sigma(6(W_2h+b_2))\f]
 * 
 */
template<typename TypeTensor=decltype(at::CUDA(at::kFloat))> 
class MLBuilderMLPSigmoid: MLBuilderMLP<TypeTensor>
{
    protected:
    
    using MLBuilderMLP<TypeTensor>::m_hiddenSize;
    using MLBuilderMLP<TypeTensor>::InitializeBias;
    using MLBuilderMLP<TypeTensor>::InitializeWeight;
    
    public:
    /**
     * @brief Constructor for Hadamard product neural network.
     * @param device Device on which to do computation.
     * @param hiddenSize Vector indicating the size of every hidden layer. Zero dimensional vector mean linear.
     */
    MLBuilderMLPSigmoid(TypeTensor& device=at::CUDA(at::kFloat),std::vector<size_t> hiddenSize=std::vector<size_t>());
    /**
     * Copy constructor.
     * @param copy Class to copy.
     */
    MLBuilderMLPSigmoid(const MLBuilderMLPSigmoid& copy)=default;


    /**
     * MLify a function.
     * @param f Function to MLify.
     * @return A FunctionOperator which contain the function and parameter.
     */
    template<typename Return,class ...Args>
    auto Build(std::function<Return(Args...)> f);
    
};

#include "MLBuilderMLPSigmoid.tpp"

