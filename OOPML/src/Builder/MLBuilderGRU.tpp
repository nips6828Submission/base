#include "MLBuilderGRU.h"
#include "torch/csrc/jit/passes/to_batch.h"

template<typename TypeTensor>
MLBuilderGRU<TypeTensor>::MLBuilderGRU(TypeTensor& device):MLBuilder<TypeTensor>(device)
{
}

template<typename TypeTensor>
torch::autograd::Variable  MLBuilderGRU<TypeTensor>::InitializeBias(size_t size)
{
    return torch::autograd::make_variable(at::zeros({static_cast<long int>(size)},m_device),true);
}

template<typename TypeTensor>
torch::autograd::Variable  MLBuilderGRU<TypeTensor>::InitializeWeight(size_t size1,size_t size2)
{
    double bound=sqrt(6.0)/(size1+size2);
    return torch::autograd::make_variable(at::zeros({static_cast<long int>(size1),static_cast<long int>(size2)},m_device).uniform_(-bound,bound),true);
}

template<typename TypeTensor>
template<typename Return,class ...Args>
auto MLBuilderGRU<TypeTensor>::Build(std::function<Return(Return,Args...)> f)
{
    auto CreateStack=[&]()
    {
        torch::jit::Stack stackParam;
        size_t previousSize=(0 + ... + ML<Args>::size());
        stackParam.push_back(InitializeWeight(3*ML<Return>::size(),previousSize));
        stackParam.push_back(InitializeWeight(3*ML<Return>::size(),ML<Return>::size()));
        stackParam.push_back(InitializeBias(3*ML<Return>::size()));
        stackParam.push_back(InitializeBias(3*ML<Return>::size()));
        return stackParam;
    };
    auto CreateGraph=[&](bool Batch)
    {
        auto graph=std::make_shared<torch::jit::Graph>();
        auto state=torch::jit::SymbolicVariable::asNewInput(*graph);
        std::vector<torch::jit::SymbolicVariable> SymbolicInputs=GenerateInputSymbolic<sizeof...(Args)>(graph);
        int dim=1;
        if(!Batch)
        {
            dim=0;
        }
        torch::jit::SymbolicVariable input;
        if constexpr (sizeof...(Args)>1)
        {
            input=torch::jit::SymbolicVariable::cat(SymbolicInputs,dim);
        }
        else
        {
            if(!Batch)
            {
                input=SymbolicInputs[0].unsqueeze(0);
            }
            else
            {
                input=SymbolicInputs[0];
            }
        }
        if(!Batch)
        {
            state=state.unsqueeze(0);
        }
    
        //Create the parameter symbolic Variable.
        auto w_ih=torch::jit::SymbolicVariable::asNewInput(*graph);
        auto w_hh=torch::jit::SymbolicVariable::asNewInput(*graph);
        auto b_ih=torch::jit::SymbolicVariable::asNewInput(*graph);
        auto b_hh=torch::jit::SymbolicVariable::asNewInput(*graph);
        
        //Apply the gru.
        auto result=torch::jit::SymbolicVariable::create(torch::jit::Symbol::aten("gru_cell"),{input,state,w_ih,w_hh,b_ih,b_hh})[0];
        if(!Batch)
        {
            result=result.squeeze(0);
        }
        result.addAsOutput();
        graph->lint();
        return graph;
    };
    torch::jit::Stack stackParam=CreateStack();
    auto graph=CreateGraph(true);
    auto graphNoBatch=CreateGraph(false);
    return FunctionOp<std::function<ML<Return>(ML<Return>,ML<Args>...)>>(graph,graphNoBatch,stackParam,f);
}

