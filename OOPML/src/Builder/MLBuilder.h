#pragma once
#include <ATen/ATen.h>
#include "Core/ML.h"
#include "functional"
#include <deque>
#include <typeindex>
#include "torch/csrc/jit/symbolic_variable.h"
#include "torch/csrc/jit/graph_executor.h"
#include "Core/FunctionOp.h"
#include <cmath>
#include <exception>


/**
 * @brief Abstract class which MLify functions.
 * 
 * The idea is that from a function signature we build a neural network compatible with the signature. This is done with the Build member function.
 * 
 * @tparam TypeTensor Type of tensor default on gpu tensor.
 */
template<typename TypeTensor=decltype(at::CUDA(at::kFloat))>
class MLBuilder
{
protected:
    /**
     * @brief Device.
     */
    TypeTensor& m_device;
public:
    /**
     * Construct the builder.
     * @param device Device to use.
     */
    inline MLBuilder(TypeTensor& device=at::CUDA(at::kFloat));
    /**
     * Copy constructor.
     * @param copy Class to copy.
     */
    inline MLBuilder(const MLBuilder& copy);


    /**
     * MLify a function.
     * @param f Function to MLify.
     * @return A FunctionOperator which contain the function and parameter.
     */
    template<typename Return,class ...Args>
    auto Build(std::function<Return(Args...)> f)
    {
        throw std::logic_error("Calling Build of MLBuilder directly. Only inherited version should be called.");
    }

    template <class Type>
    auto GetML(const Type& val);
    template <class Type,class Tensor>
    auto GetML(const Type& val,Tensor ten);
};

/**
 * @brief Utility function to generate a vector of symbolic variable of a given size.
 * @tparam dim Dimension of the vector.
 * @param graph Graph to use.
 * @return A vector of symbolic variable of the given size.
 */
template<size_t dim>
std::vector<torch::jit::SymbolicVariable> GenerateInputSymbolic(std::shared_ptr<torch::jit::Graph> graph)
{
    std::vector<torch::jit::SymbolicVariable> res;
    for(size_t i=0;i<dim;++i)
    {
        res.push_back(torch::jit::SymbolicVariable::asNewInput(*graph));
    }
    return res;
}

#include "Builder/MLBuilder.tpp"
