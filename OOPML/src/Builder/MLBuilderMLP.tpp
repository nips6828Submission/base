#include "MLBuilderMLP.h"

template<typename TypeTensor>
MLBuilderMLP<TypeTensor>::MLBuilderMLP(TypeTensor& device,std::vector<size_t> hiddenSize):MLBuilder<TypeTensor>(device),m_hiddenSize(hiddenSize)
{
}

template<typename TypeTensor>
torch::autograd::Variable  MLBuilderMLP<TypeTensor>::InitializeBias(size_t size)
{
    return torch::autograd::make_variable(at::zeros({static_cast<long int>(size)},m_device),true);
}

template<typename TypeTensor>
torch::autograd::Variable  MLBuilderMLP<TypeTensor>::InitializeWeight(size_t size1,size_t size2)
{
    double bound=sqrt(6.0)/(size1+size2);
    return torch::autograd::make_variable(at::zeros({static_cast<long int>(size1),static_cast<long int>(size2)},m_device).uniform_(-bound,bound),true);
}

template<typename TypeTensor>
template<typename Return,class ...Args>
auto MLBuilderMLP<TypeTensor>::Build(std::function<Return(Args...)> f)
{
    auto CreateStack=[&]()
    {
        torch::jit::Stack stackParam;
        // Check if there is no argument, in this case we simply learn a constant vector.
        if constexpr(sizeof...(Args)==0)
        {
            stackParam.push_back(InitializeBias(ML<Return>::size()));
            return stackParam;
        }
        
        size_t previousSize=(0 + ... + ML<Args>::size());
        //Loop throught all hidden layer.
        for(int i=0;i<m_hiddenSize.size();++i)
        {
            size_t currentSize=m_hiddenSize[i];
            stackParam.push_back(InitializeWeight(currentSize,previousSize));
            stackParam.push_back(InitializeBias(currentSize));
            previousSize=currentSize;
        }
        size_t currentSize=ML<Return>::size();
        stackParam.push_back(InitializeWeight(currentSize,previousSize));
        stackParam.push_back(InitializeBias(currentSize));
        return stackParam;
    };
    
    auto CreateGraph=[&](auto Batch)
    {
        auto graph=std::make_shared<torch::jit::Graph>();    
        // Check if there is no argument, in this case we simply learn a constant vector.
        if constexpr(sizeof...(Args)==0)
        {
            torch::jit::SymbolicVariable result=torch::jit::SymbolicVariable::asNewInput(*graph);
            result.addAsOutput();
            graph->lint();
            return graph;
        }
    
        std::vector<torch::jit::SymbolicVariable> SymbolicInputs=GenerateInputSymbolic<sizeof...(Args)>(graph);
        int dim=1;
        if(!Batch)
        {
            dim=0;
        }
        torch::jit::SymbolicVariable result;
        if constexpr(sizeof...(Args)==1)
        {
            result=SymbolicInputs[0];
        }
        else
        {
            result=torch::jit::SymbolicVariable::cat(SymbolicInputs,dim);
        }
        //Obtain the total size of the argument.
        size_t previousSize=(0 + ... + ML<Args>::size());
        //Loop throught all hidden layer.
        for(int i=0;i<m_hiddenSize.size();++i)
        {
            auto w=torch::jit::SymbolicVariable::asNewInput(*graph);
            auto b=torch::jit::SymbolicVariable::asNewInput(*graph);
            result=b+torch::jit::SymbolicVariable::create(torch::jit::Symbol::aten("mv"),{w,result})[0];
            result=torch::jit::SymbolicVariable::create(torch::jit::Symbol::aten("relu"),{result})[0];
        }
        auto w=torch::jit::SymbolicVariable::asNewInput(*graph);
        auto b=torch::jit::SymbolicVariable::asNewInput(*graph);
        result=b+torch::jit::SymbolicVariable::create(torch::jit::Symbol::aten("mv"),{w,result})[0];
        result.addAsOutput();
        graph->lint();
        return graph;
    };

    auto graph=CreateGraph(true);
    auto graphNoBatch=CreateGraph(false);
    torch::jit::Stack stackParam=CreateStack();
    return FunctionOp<std::function<ML<Return>(ML<Args>...)>>(graph,graphNoBatch,stackParam,f);
}
