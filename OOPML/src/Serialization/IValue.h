#pragma once
#include "torch/csrc/jit/ivalue.h"

namespace boost {
namespace serialization {
template<class Archive>
void serialize(Archive & ar,torch::jit::IValue & v, const unsigned int version)
{
    if(v.isTensor())
    {
        auto tensor=v.toTensor();
        ar & tensor;
    }
}
}
}
