#pragma once
#include "torch/csrc/api/include/torch/serialize.h"
#include <torch/all.h>

namespace boost {
namespace serialization {
template<class Archive>
void serialize(Archive & ar,const at::Tensor & T, const unsigned int version)
{
    if constexpr (Archive::is_saving::value)
    {
        using torch::operator<<;
        torch::serialize::OutputArchive oa;
        oa <<T;
    }
    else
    {
    }
}
}
}
