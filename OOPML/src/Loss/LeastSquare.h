#pragma once
#include <Core/FunctionOp.h>

template<typename Type>
inline FunctionOp<std::function<ML<double>(ML<Type>,ML<Type>)>> LeastSquareLoss(std::function<double(Type Prediction,Type Target)> f)
{
    auto graph=std::make_shared<torch::jit::Graph>();
    torch::jit::SymbolicVariable VarPrediction=torch::jit::SymbolicVariable::asNewInput(*graph);
    torch::jit::SymbolicVariable VarTarget=torch::jit::SymbolicVariable::asNewInput(*graph);
    auto square=(VarPrediction-VarTarget)*(VarPrediction-VarTarget);
    auto result=square.sum();
    result.addAsOutput();
    torch::jit::Stack param;
    return FunctionOp<std::function<ML<double>(ML<Type>,ML<Type>)>>(graph,graph,param,f);
};
