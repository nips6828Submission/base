#pragma once
#include "Type/BinaryFloat.h"
#include "Core/FunctionOp.h"
#include <cmath> 

inline FunctionOp<std::function<ML<double>(ML<BinaryFloat>)>> ToDoubleGraph()
{
    auto graph=std::make_shared<torch::jit::Graph>();
    torch::jit::SymbolicVariable VarPrediction=torch::jit::SymbolicVariable::asNewInput(*graph);
    auto weightTen=at::empty({32},at::kCUDA);
    for(int i=0;i<32;++i)
    {
        weightTen[i]=pow(0.5,32-i-10);
    }
    
    torch::jit::SymbolicVariable weight=graph->insertConstant(weightTen);
    auto ToDouble=[&](auto variable)
    {
        auto res=variable*weight;
        return -(512)+res.sum();
    };
    auto PredictionDouble=ToDouble(VarPrediction);
    PredictionDouble.addAsOutput();
    torch::jit::Stack param;
    std::function<double(BinaryFloat)> f=[](BinaryFloat Prediction)
    {
        double pred=Prediction;
        return pred;
    };
    return FunctionOp<std::function<ML<double>(ML<BinaryFloat>)>>(graph,graph,param,f);
};

inline FunctionOp<std::function<ML<double>(ML<BinaryFloat>,ML<BinaryFloat>)>> LeastSquareLossBinary()
{
    auto graph=std::make_shared<torch::jit::Graph>();
    torch::jit::SymbolicVariable VarPrediction=torch::jit::SymbolicVariable::asNewInput(*graph);
    torch::jit::SymbolicVariable VarTarget=torch::jit::SymbolicVariable::asNewInput(*graph);
    auto weightTen=at::empty({32},at::kCUDA);
    for(int i=0;i<32;++i)
    {
        weightTen[i]=pow(0.5,32-i-10);
    }
    
    torch::jit::SymbolicVariable weight=graph->insertConstant(weightTen);
    auto ToDouble=[&](auto variable)
    {
        auto res=variable*weight;
        return -(512)+res.sum();
    };
    auto PredictionDouble=ToDouble(VarPrediction);
    auto TargetDouble=ToDouble(VarTarget);

    auto diff=PredictionDouble-TargetDouble;
    auto result2=VarPrediction-VarTarget;
    auto error=diff*diff;
    auto error2=result2*result2*weight;
    error=error2.sum();
    error.addAsOutput();
    torch::jit::Stack param;
    std::function<double(BinaryFloat,BinaryFloat)> f=[](BinaryFloat Prediction,BinaryFloat Target)
    {
        double pred=Prediction;
        double target=Target;
        double diff=pred-target;
        return diff*diff;
    };
    return FunctionOp<std::function<ML<double>(ML<BinaryFloat>,ML<BinaryFloat>)>>(graph,graph,param,f);
};
