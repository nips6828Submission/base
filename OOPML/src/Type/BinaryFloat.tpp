#include "BinaryFloat.h"
#include <cnl/all.h>

BinaryFloat::BinaryFloat(double value)
{
    cnl::fixed_point<uint, -22> fixed(value+512);
    auto rep=to_rep(fixed);
    m_bit=std::bitset<32>(to_rep(fixed));
}

BinaryFloat::operator double()
{
    ulong value=m_bit.to_ulong();
    cnl::from_rep<cnl::fixed_point<uint, -22>,uint> func;
    auto fixed=func(value);
    double finalValue=static_cast<double>(fixed);
    return finalValue-512;
}
