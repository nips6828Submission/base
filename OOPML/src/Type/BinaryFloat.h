#pragma once
#include <bitset>
#include "Core/ML.h"

/**
 * @brief BinaryFloat is a class which represent a float with a fixed point binary representation.
 * It contain implicit conversion from and to float.
 */
class BinaryFloat
{
    /**
     * @brief internal representation as bits.
     */
    std::bitset<32> m_bit;
public:
    friend auto GenerateMLValue(BinaryFloat res,at::Tensor tensor);
    template <typename Accessor>
    friend void GenerateMLValue(BinaryFloat res,Accessor tensor);
    /**
     * @brief Constructor from value;
     */
    BinaryFloat(double value=0);
    /**
     * @brief Conversion to number.
     */
    operator double();
};

#include "BinaryFloat.tpp"

template <>
class ML<BinaryFloat>: public at::Tensor
{
    public:
    static size_t m_size;
    static size_t size()
    {
        return m_size;
    }
    ML()
    {
    }
    ML(at::Tensor embedded):at::Tensor(embedded)
    {
        
    }
    static constexpr bool IsFloatCompatible()
    {
        return false;
    }
    typedef BinaryFloat Type;
};

size_t ML<BinaryFloat>::m_size=32;

auto GenerateMLValue(BinaryFloat res,at::Tensor tensor)
{
    for(size_t i=0;i<32;++i)
    {
        bool bit=res.m_bit[i];
        tensor[i]=double(bit);
    }
    return ML<BinaryFloat>(tensor);
};

template <typename Accessor>
void GenerateMLValue(BinaryFloat res,Accessor tensor)
{
    for(size_t i=0;i<32;++i)
    {
        bool bit=res.m_bit[i];
        tensor[i]=double(bit);
    }
};
