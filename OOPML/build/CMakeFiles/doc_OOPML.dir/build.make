# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.13

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/local/bin/cmake

# The command to remove a file.
RM = /usr/local/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /workspace/OOPML

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /workspace/OOPML/build

# Utility rule file for doc_OOPML.

# Include the progress variables for this target.
include CMakeFiles/doc_OOPML.dir/progress.make

CMakeFiles/doc_OOPML:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/workspace/OOPML/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Generating API documentation with Doxygen"
	/usr/local/bin/doxygen /workspace/OOPML/build/Doxyfile

doc_OOPML: CMakeFiles/doc_OOPML
doc_OOPML: CMakeFiles/doc_OOPML.dir/build.make

.PHONY : doc_OOPML

# Rule to build all files generated by this target.
CMakeFiles/doc_OOPML.dir/build: doc_OOPML

.PHONY : CMakeFiles/doc_OOPML.dir/build

CMakeFiles/doc_OOPML.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/doc_OOPML.dir/cmake_clean.cmake
.PHONY : CMakeFiles/doc_OOPML.dir/clean

CMakeFiles/doc_OOPML.dir/depend:
	cd /workspace/OOPML/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /workspace/OOPML /workspace/OOPML /workspace/OOPML/build /workspace/OOPML/build /workspace/OOPML/build/CMakeFiles/doc_OOPML.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/doc_OOPML.dir/depend

