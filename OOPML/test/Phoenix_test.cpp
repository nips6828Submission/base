#include <gtest/gtest.h>
#include <iostream>
#include <type_traits>
#include <boost/phoenix/core.hpp>
#include <boost/phoenix/stl.hpp>
#include <boost/phoenix/core/argument.hpp>
#include <boost/phoenix/function.hpp>
#include <vector>
#include <boost/phoenix/operator.hpp>
#include <boost/phoenix/object/construct.hpp>
#include <typeindex>
#include <any>

template <typename T>
struct ApplyFunction
{
};


class TypeGenerator
{
public:
    operator std::vector<int>()
    {
        std::vector<int> v;
        v.push_back(1);
        return v;
    }
    operator int()
    {
        std::cout<<"int "<<std::endl;
        return 1;
    }
    operator double()
    {
        std::cout<<"double "<<std::endl;
        return 2;
    }
    
    template <typename T>
    auto Conversion() -> decltype(ApplyFunction<T>::f(TypeGenerator(),TypeGenerator(),TypeGenerator()))
    {
        std::cout<<"doing conversion"<<std::endl;
        return ApplyFunction<T>::f(TypeGenerator(),TypeGenerator(),TypeGenerator());
    };
    template <typename T>
    friend T operator+(TypeGenerator lhs,T rhs) // otherwise, both parameters may be const references
    {
        return static_cast<T>(lhs)+rhs;
    }
    
    friend double operator+(TypeGenerator lhs,TypeGenerator rhs) // otherwise, both parameters may be const references
    {
        return static_cast<double>(lhs)+static_cast<double>(rhs);
    }
};

class MyClass
{
    int m_i;
public:
    MyClass(int i):m_i(i)
    {
        std::cout<<"creating my class "<<i<<std::endl;
    }
    MyClass(const MyClass& M):m_i(M.m_i)
    {
        std::cout<<"copy constructor"<<std::endl;
    }
    MyClass(TypeGenerator T)
    {
        std::cout<<"type generator"<<std::endl;
    }
    double Mult(double v)
    {
        std::cout<<"multiplying"<<std::endl;
        m_i=v*m_i;
        return v*m_i; 
    }
};



BOOST_PHOENIX_ADAPT_FUNCTION(double, Mult, std::function<double(MyClass,double)>(&MyClass::Mult), 2)
//BOOST_PHOENIX_ADAPT_FUNCTION(MyClass, Construct, &MyClass::MyClass, 1)

template <>
struct ApplyFunction<MyClass>
{
    static decltype(boost::phoenix::construct<MyClass>(TypeGenerator())) f;
    //static decltype(Construct(boost::phoenix::arg_names::arg1)) f;
};

decltype(boost::phoenix::construct<MyClass>(TypeGenerator())) ApplyFunction<MyClass>::f=boost::phoenix::construct<MyClass>(TypeGenerator());

//decltype(Construct(boost::phoenix::arg_names::arg1)) ApplyFunction<MyClass>::f=Construct(boost::phoenix::arg_names::arg1);

template <>
struct ApplyFunction<double>
{
    static decltype(Mult(boost::phoenix::arg_names::arg1,boost::phoenix::arg_names::arg2)) f;
};

decltype(Mult(boost::phoenix::arg_names::arg1,boost::phoenix::arg_names::arg2)) ApplyFunction<double>::f=Mult(boost::phoenix::arg_names::arg1,boost::phoenix::arg_names::arg2);



TEST(Phoenix, example)
{
    using boost::phoenix::arg_names::arg1;
    using boost::phoenix::arg_names::arg2;
    //auto f=&MyClass::Mult;
    
    TypeGenerator T;
    //auto v=static_cast<MyClass>(T);
    //auto v=T.Conversion<double>();
    MyClass M(2.0);
    T.Conversion<MyClass>();
    
}

TEST(Phoenix, example2)
{
    std::multimap<std::type_index,std::any> mymap;
    double d=0;
    mymap.insert({std::type_index(typeid(double)),std::any(d)});
}

