#include "Core/StartUp.h"
StartUp start;
#include <gtest/gtest.h>
#include "torch/csrc/autograd/variable.h"
#include "torch/csrc/jit/symbolic_variable.h"
#include "torch/csrc/jit/autodiff.h"
#include "torch/csrc/jit/graph_executor.h"
#include "torch/csrc/autograd/function.h"
#include "torch/csrc/jit/passes/to_batch.h"
#include "torch/csrc/jit/batched/BatchTensor.h"
#include "torch/csrc/jit/custom_operator.h"

#include "Core/Backward.h"
#include <functional>
#include "Core/FunctionOp.h"

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <fstream>
#include <boost/serialization/vector.hpp>
#include "Serialization/Tensor.h"

#include "Core/ML.h"



struct SymbolicVariable {
  SymbolicVariable() : v(nullptr) {}
  SymbolicVariable(SymbolicVariable &&)=default;
  SymbolicVariable(torch::jit::Value * v) : v(v) {
      std::cout<<"construct"<<std::endl;
}
 ~SymbolicVariable()
 {
    std::cout<<"destruct"<<std::endl;
 }
  torch::jit::Value* v;
    void addAsOutput() const {
    v->owningGraph()->registerOutput(v);
  }
  SymbolicVariable operator=(SymbolicVariable other)
  {
      return SymbolicVariable(other.v);
  }
};

TEST(Fusion,TestGraphExecutor)
{
    try
    {
    auto graph=std::make_shared<torch::jit::Graph>();
    torch::jit::SymbolicVariable i0 = torch::jit::SymbolicVariable::asNewInput(*graph);
    torch::jit::SymbolicVariable i1 = torch::jit::SymbolicVariable::asNewInput(*graph);
    auto o0 = i0.mm(i1);
    o0.addAsOutput();
    graph->dump();
    torch::jit::GraphExecutor executor(graph,true);
    auto ten_a=at::rand({3,4}, at::kCUDA);
    auto ten_b=at::rand({4,3}, at::kCUDA);
    auto a=torch::autograd::make_variable(ten_a,true);
    auto b=torch::autograd::make_variable(ten_b,true);
    torch::jit::Stack stack({a,b});
    executor.run(stack);
    std::cout<<"stack size "<<stack.size()<<std::endl;
    torch::autograd::Variable output(stack[0].toTensor());
    auto gradOutput=torch::autograd::make_variable(at::ones({3,3}, at::kCUDA),false);
    Backward(output,gradOutput);
    std::cout<<a.grad()<<std::endl;
    std::cout<<b.grad()<<std::endl;

    auto a2=torch::autograd::make_variable(ten_a,true);
    auto b2=torch::autograd::make_variable(ten_b,true);
    torch::autograd::Variable res = a2.mm(b2);
    Backward(res,gradOutput);
    std::cout<<a2.grad()<<std::endl;
    std::cout<<b2.grad()<<std::endl;

  

    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}

/*
TEST(Fusion,TestGraphExecutorIfBatch)
{
    try
    {        
        auto graph=std::make_shared<torch::jit::Graph>();
        torch::jit::SymbolicVariable inputcond = torch::jit::SymbolicVariable::asNewInput(*graph);
        
        auto outputcond = inputcond>0.0;
        auto boolType=torch::jit::TensorType::create(at::zeros({1}, at::kCUDA)<0);
        outputcond.value()->setType(boolType);
        //outputcond.value()->setType(torch::jit::FloatType::get());
        torch::jit::SymbolicVariable input = torch::jit::SymbolicVariable::asNewInput(*graph);
        auto n0=graph->insertNode(graph->createTensorToNum(torch::jit::IntType::get(),outputcond));
        auto outputcondint=n0->outputs()[0];
        auto n=graph->insertNode(graph->create(torch::jit::prim::If,1));
        n->addInput(outputcondint);
        auto n2=n->addBlock();
        n2->registerOutput(input.value());
        auto n3=n->addBlock();
        auto newnode=n3->appendNode(graph->create(torch::jit::aten::neg,1));
        newnode->addInput(input.value());
        n3->registerOutput(newnode->outputs()[0]);
        graph->registerOutput(n->outputs()[0]);
        
        auto new_graph=to_batch_graph(graph);
        
        new_graph->dump();
        new_graph->lint();
        /*
        torch::jit::GraphExecutor executor(graph,true);
        auto ten_a=at::zeros({1}, at::kCUDA);
        auto ten_b=at::zeros({1}, at::kCUDA);
        ten_a[0]=-20.0;
        ten_b[0]=10;
        auto a=torch::autograd::make_variable(ten_a,true);
        auto b=torch::autograd::make_variable(ten_b,true);
        torch::jit::Stack stack({a,b});
        std::cout<<"bef execute"<<std::endl;
        executor.run(stack);
        std::cout<<"stack size "<<stack.size()<<std::endl;
        std::cout<<"stack[0] "<<stack[0]<<std::endl;
        torch::autograd::Variable res=stack[0].toTensor();
        std::cout<<"res "<<res<<std::endl;
        executor.graph()->dump();
        torch::jit::Stack stack2({a,b});
        executor.graphFor(stack2)->dump();
        */
        /*
    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}
*/
        

TEST(Fusion,TestFuncOpInterfaceWithParamCallFunc)
{
    try
    {
        auto graphfunc=std::make_shared<torch::jit::Graph>();
        torch::jit::SymbolicVariable inputa = torch::jit::SymbolicVariable::asNewInput(*graphfunc);
        torch::jit::SymbolicVariable inputb = torch::jit::SymbolicVariable::asNewInput(*graphfunc);
        auto output=inputa-inputb;
        output.addAsOutput();
        auto ten_b=at::zeros({5}, at::kCUDA);
        auto b=torch::autograd::make_variable(ten_b,true);
        torch::jit::Stack param({b});
        FunctionOp<std::function<ML<at::Tensor>(ML<at::Tensor>)>> funcOp(graphfunc,graphfunc,param);
        
        auto graph=std::make_shared<torch::jit::Graph>();
        torch::jit::SymbolicVariable asymb = torch::jit::SymbolicVariable::asNewInput(*graph);
        auto paramsymb=funcOp.CreateSymbolicParameters(graph);
        auto outputfinal=funcOp.InsertNode(graph,paramsymb,{asymb});
        outputfinal[0].addAsOutput();
        
        graph->dump();
        
        auto ten_a=at::zeros({5}, at::kCUDA);
        auto a=torch::autograd::make_variable(ten_a,true);

        auto myfunc=funcOp.GetFunction();
        auto res=myfunc(a);
        std::cout<<"res "<<res<<std::endl;
    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}

TEST(Fusion,TestEmptyFunction)
{
    try
    {
        auto graphfunc=std::make_shared<torch::jit::Graph>();
        torch::jit::SymbolicVariable inputa = torch::jit::SymbolicVariable::asNewInput(*graphfunc);
        torch::jit::SymbolicVariable inputb = torch::jit::SymbolicVariable::asNewInput(*graphfunc);
        auto output=inputa-inputb;
        output.addAsOutput();
        auto ten_a=at::zeros({5}, at::kCUDA);
        auto a=torch::autograd::make_variable(ten_a,true);
        auto ten_b=at::zeros({5}, at::kCUDA);
        auto b=torch::autograd::make_variable(ten_b,true);
        torch::jit::Stack param({a,b});
        FunctionOp<std::function<ML<at::Tensor>()>> funcOp(graphfunc,graphfunc,param);
        
        auto graph=std::make_shared<torch::jit::Graph>();
        //torch::jit::SymbolicVariable asymb = torch::jit::SymbolicVariable::asNewInput(*graph);
        auto paramsym=funcOp.CreateSymbolicParameters(graph);
        auto outputfinal=funcOp.InsertNode(graph,paramsym,{});
        outputfinal[0].addAsOutput();
        
        graph->dump();

        auto myfunc=funcOp.GetFunction();
        auto res=myfunc();
        std::cout<<"res "<<res<<std::endl;
    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}

/*TEST(Fusion,TestFuncOpInterfaceSaveLoad)
{
    try
    {
        std::ofstream ofs(std::string("test_save_Fusion1"));
        boost::archive::binary_oarchive ar(ofs);
        auto graphfunc=std::make_shared<torch::jit::Graph>();
        torch::jit::SymbolicVariable inputa = torch::jit::SymbolicVariable::asNewInput(*graphfunc);
        torch::jit::SymbolicVariable inputb = torch::jit::SymbolicVariable::asNewInput(*graphfunc);
        auto output=inputa-inputb;
        output.addAsOutput();
        torch::jit::Stack param;
        FunctionOp<std::function<at::Tensor(at::Tensor,at::Tensor)>> funcOp(graphfunc,param);
        auto praram=funcOp.GetParamRef();
        ar<<praram;
    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}
*/
