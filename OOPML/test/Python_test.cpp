#include <gtest/gtest.h>
#include <iostream>
#include <string>
#include <boost/python.hpp>
#include "Core/Pytorch.h"

using namespace boost::python;

TEST(Python,greet)
{
    //Initialize python.
    Py_Initialize();
    // Retrieve the main module.
    object main = import("__main__");

    // Retrieve the main module's namespace
    object global(main.attr("__dict__"));

    // Define greet function in Python.
    object result = exec(
    "def greet():                   \n"
    "   return 'Hello from Python!' \n",
    global, global);

    // Create a reference to it.
    object greet = global["greet"];

    // Call it.
    std::string message = extract<std::string>(greet());
    std::cout << message << std::endl;
}


TEST(PyTorch,python)
{
    //Initialize python.
    std::cout<<"python"<<std::endl;
    Py_Initialize();
    // Retrieve the main module.
    object main = import("__main__");

    // Retrieve the main module's namespace
    object global(main.attr("__dict__"));
    // Retrieve the main module.
    try
    {
    object result = exec(
    "import sys\n"
    "print(sys.version_info[0])",
    global, global);
    }
    catch(error_already_set const &)
    {
        PyErr_Print();
    }
}

TEST(PyTorch,Tensor)
{
    //Initialize python.
    std::cout<<"tensor"<<std::endl;
    Py_Initialize();
    // Retrieve the main module.
    try
    {
    std::cout<<"bef python"<<std::endl;
    object torch = import("torch");
    std::cout<<"aft tensor"<<std::endl;
    object randn=torch.attr("randn");
    object a=randn(5,7);
    object b=randn(5,7);
    object c=a+b;
    std::string tensora = extract<std::string>(str(a));
    std::string tensorb = extract<std::string>(str(b));
    std::string tensorc = extract<std::string>(str(c));
    std::cout<<"a "<<tensora<<std::endl;
    std::cout<<"b "<<tensorb<<std::endl;
    std::cout<<"c "<<tensorc<<std::endl;
    }
    catch(error_already_set const &)
    {
        PyErr_Print();
    }
}

TEST(PyTorch,AutoGrad)
{
    //Initialize python.
    Py_Initialize();
    try
    {
        object torch = import("torch");
        object ones=torch.attr("ones");
        object strided=torch.attr("strided");
        object x=ones(make_tuple(2,2),object(),object(),strided,object(),true);
        object y=x*x*x+2;
        y.attr("backward")(x,object(),true);
        std::string message = extract<std::string>(str(x.attr("grad")));
        std::cout<<"message "<<message<<std::endl;
    }
    catch(error_already_set const &)
    {
        PyErr_Print();
    }
}

TEST(PyTorch,Adam)
{
    //Initialize python.
    Py_Initialize();
    try
    {
        object torch = import("torch");
        object ones=torch.attr("ones");
        object strided=torch.attr("strided");
        object x=ones(make_tuple(2,2),object(),object(),strided,object(),true);
        auto func=[](object& x)
        {
            return (x-2)*(x-2);
        };
        
        object optim=torch.attr("optim");
        object Adam=optim.attr("Adam");
        object y=func(x);
        list l;
        l.append(x);
        object Optimizer=Adam(l);
        std::cout<<"optim ready"<<std::endl;
        for(int i=0;i<=100000;++i)
        {
            Optimizer.attr("zero_grad")();
            object output=func(x);
            output.attr("backward")(ones(make_tuple(2,2)));
            Optimizer.attr("step")();
        }
            std::string message = extract<std::string>(str(x));
            std::cout<<message<<std::endl;
    }
    catch(error_already_set const &)
    {
        PyErr_Print();
    }
}

TEST(PyTorch,list)
{
    
}
