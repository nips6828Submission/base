#include <gtest/gtest.h>
#include <cnl/all.h>

/**
 * \test { Check that cnl work as advertised. That it transform a float to an int in fixed representation. }
 */
TEST(cnl,fixed_point)
{
    std::cout<<sizeof(long)<<std::endl;
    auto x = cnl::fixed_point<long, -32>{3.1434592};
    std::cout << to_rep(x) << std::endl;  // "7"
    std::cout << x << std::endl;  // "3.5"
}
