#include <gtest/gtest.h>
#include <Builder/MLBuilder.h>
#include <Builder/MLBuilderMLP.h>
#include <Builder/MLBuilderGRU.h>
#include "Core/StartUp.h"
#include <random>
#include <chrono>

StartUp start;



TEST(MLBuilder,add)
{
    MLBuilderMLP builder;
    std::function<double(double,double)> fadd=[](double v1,double v2)
    {
        return v1+v2;
    };
    auto mlFunctionOp=builder.Build(fadd);
    auto mlfunc=mlFunctionOp.GetFunction();
    try
    {
        auto ten=at::zeros({1},at::kCUDA);
        ten[0]=1;
        torch::autograd::Variable oneTensor=torch::autograd::make_variable(ten);
        ML<double> v1(oneTensor);
        ML<double> v2(oneTensor);
        auto res=mlfunc(v1,v2);
        std::cout<<res<<std::endl;
        double resd=static_cast<double>(res);
        std::cout<<resd<<std::endl;
        double resd2=static_cast<double>(mlfunc(v1,v2));
        std::cout<<resd2<<std::endl;
    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}

TEST(MLBuilder,pow)
{
    MLBuilderMLP builder;
    std::function<double(double,double,int)> fpow=[](double v1,double v2,int v3)
    {
        return pow(v1+v2,v3);
    };
    auto mlFunctionOp=builder.Build(fpow);
    auto mlfunc=mlFunctionOp.GetFunction();
    try
    {
        auto ten=at::zeros({1},at::kCUDA);
        ten[0]=1;
        torch::autograd::Variable oneTensor=torch::autograd::make_variable(ten);
        ML<double> v1(oneTensor);
        ML<double> v2(oneTensor);
        ML<int> v3(oneTensor);
        auto res=mlfunc(v1,v2,v3);
        double resd=static_cast<double>(res);
        std::cout<<resd<<std::endl;
    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}

TEST(MLBuilder,mv)
{
    try
    {
        auto w=at::zeros({10,2},at::kCUDA);
        auto x=at::zeros({2},at::kCUDA);
        auto res=at::mv(w,x);
        std::cout<<res<<std::endl;
    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}

struct S
{
    double value;
};

TEST(MLBuilder,GRU)
{
    MLBuilderGRU builder(at::CUDA(at::kFloat));
    std::function<S(S,double)> fs=[](S s,double val)
    {
        S ret;
        ret.value=s.value+val;
        return ret;
    };
    auto mlFunctionOp=builder.Build(fs);
}

TEST(MLBuilder,GetGeneric)
{
    MLBuilderMLP builder;
    std::function<S(S,double)> fs=[](S s,double val)
    {
        S ret;
        ret.value=s.value+val;
        return ret;
    };
    auto mlFunctionOp=builder.Build(fs);
    auto mlGeneric=mlFunctionOp.GetGeneric();
}

/*
TEST(MLBuilder,BuildInser)
{
    MLBuilder builder;
    typedef std::default_random_engine type_generator;
    int seed=std::chrono::system_clock::now().time_since_epoch().count();
    type_generator generator(seed);
    std::function<long(type_generator& generator,size_t)>fgen=[](type_generator& generator,size_t depth)
    {
        std::cout<<"f3 "<<std::endl;
        std::uniform_int_distribution<long> distribution(1,10);
        auto res=distribution(generator);
        std::cout<<"res "<<res<<std::endl;
        return res;
    };
    auto mlfunc=builder.BuildInsert(fgen);
    try
    {
        long res=static_cast<long>(mlfunc(generator,0));
        std::cout<<res<<std::endl;
    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}
*/

TEST(MLBuilder,empty)
{
    MLBuilderMLP builder;
    std::function<double()> fempty=[]()
    {
        return 3.14;
    };
    auto mlFunctionOp=builder.Build(fempty);
}


/*TEST(MLBuilder,ReadWrite)
{
    MLBuilder builder(at::CUDA(at::kFloat),10,0,0);
    std::function<double(double,double)> fs=[](double s,double val)
    {
        return s+val;
    };
    auto mlFunctionOp=builder.Build(fs);
    torch::serialize::OutputArchive OA=mlFunctionOp.Save();
    OA.save_to("MLBuilder_ReadWrite_test.dat");
    
    torch::serialize::InputArchive IA;
    IA.load_from("MLBuilder_ReadWrite_test.dat");
    mlFunctionOp.Load(IA);
}*/

/*
TEST(MLBuilder,LoopWrite)
{
    auto ten=torch::autograd::make_variable(at::zeros({20,20},at::kCPU));
    for(int i=0;i<10;++i)
    {
        torch::serialize::OutputArchive OA;
        OA.write("tensor",ten);
        save_to_file(OA,std::string("Cuda")+std::to_string(i));
    }
}
*/
