#include <gtest/gtest.h>
#include "../src/Type/BinaryFloat.h"
#include "../src/Loss/LeastSquareBinary.h"

/**
 * \test { Check that conversion to and from float work. }
 */
TEST(BinaryFloat,CheckEqualNeg)
{
    double value=-1;
    BinaryFloat x(value);
    EXPECT_NEAR(x,value,0.001);
}

/**
 * \test { Check that conversion to and from float work. }
 */
TEST(BinaryFloat,CheckEqualPos)
{
    double value=1;
    BinaryFloat x(value);
    EXPECT_NEAR(x,value,0.001);
}

/**
 * \test { Check that conversion to Tensor work. }
 */
TEST(BinaryFloat,CheckTensorNeg)
{
    double value=-1;
    BinaryFloat x(value);
    auto zero=at::zeros({32},at::kCUDA);
    GenerateMLValue(x,zero);
    std::cout<<zero<<std::endl;
}

/**
 * \test { Check that conversion to Tensor work. }
 */
TEST(BinaryFloat,CheckTensorPos)
{
    double value=1;
    BinaryFloat x(value);
    auto zero=at::zeros({32},at::kCUDA);
    GenerateMLValue(x,zero);
    std::cout<<zero<<std::endl;
}
