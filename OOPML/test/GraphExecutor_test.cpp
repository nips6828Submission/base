#include "Core/StartUp.h"
StartUp start;
#include <gtest/gtest.h>
#include "torch/csrc/autograd/variable.h"
#include "torch/csrc/jit/symbolic_variable.h"
#include "torch/csrc/jit/autodiff.h"
#include "torch/csrc/jit/graph_executor.h"
#include "torch/csrc/autograd/function.h"
#include "torch/csrc/jit/passes/to_batch.h"
#include "torch/csrc/jit/batched/BatchTensor.h"
#include "torch/csrc/jit/custom_operator.h"

#include "Core/Backward.h"
#include <functional>
#include "Core/FunctionOp.h"

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <fstream>
#include <boost/serialization/vector.hpp>
#include "Serialization/Tensor.h"
#include "Core/ML.h"

TEST(GraphExecutor,TestFuncOpInterfaceWithParamCallFunc)
{
    try
    {
        auto graphfunc=std::make_shared<torch::jit::Graph>();
        torch::jit::SymbolicVariable inputa = torch::jit::SymbolicVariable::asNewInput(*graphfunc);
        torch::jit::SymbolicVariable inputb = torch::jit::SymbolicVariable::asNewInput(*graphfunc);
        auto output=inputa-inputb;
        output.addAsOutput();
        auto ten_b=at::zeros({5}, at::kCUDA);
        auto b=torch::autograd::make_variable(ten_b,true);
        torch::jit::Stack param({b});
        FunctionOp<std::function<ML<at::Tensor>(ML<at::Tensor>)>> funcOp(graphfunc,graphfunc,param);
        
        auto graph=std::make_shared<torch::jit::Graph>();
        torch::jit::SymbolicVariable asymb = torch::jit::SymbolicVariable::asNewInput(*graph);
        auto paramsymb=funcOp.CreateSymbolicParameters(graph);
        auto outputfinal=funcOp.InsertNode(graph,paramsymb,{asymb});
        outputfinal[0].addAsOutput();
        
        torch::jit::GraphExecutor exec(graphfunc,true);
                
        auto ten_a=at::zeros({5}, at::kCUDA);
        auto a=torch::autograd::make_variable(ten_a,true);
        
        torch::jit::Stack stack({a});
        stack.insert(stack.end(),param.begin(),param.end());
        
        exec.run(stack);
        
        auto debug=exec.getDebugState();
        
        std::for_each(debug.execution_plans.begin(),debug.execution_plans.end(),[](auto elem)
        {
            std::cout<<"key "<<elem.first.hashCode()<<std::endl;
            std::cout<<"output "<<*(elem.second.code);
        }
        );
   }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}
