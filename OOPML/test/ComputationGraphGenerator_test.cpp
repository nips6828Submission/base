#include "Core/StartUp.h"
StartUp start;
#include <gtest/gtest.h>
#include "torch/csrc/autograd/variable.h"
#include "torch/csrc/jit/symbolic_variable.h"
#include "torch/csrc/jit/autodiff.h"
#include "torch/csrc/jit/graph_executor.h"
#include "torch/csrc/autograd/function.h"
#include "torch/csrc/jit/passes/to_batch.h"
#include "torch/csrc/jit/batched/BatchTensor.h"
#include "torch/csrc/jit/custom_operator.h"
#include "Core/ExpressionGenerator.h"
#include "Core/GenerateMLValue.h"
#include "Core/ComputationGraphGenerator.h"

#include "Core/Backward.h"
#include <functional>
#include "Core/FunctionOp.h"

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <fstream>
#include <boost/serialization/vector.hpp>
#include "Serialization/Tensor.h"

#include "Core/Symbolic_Variable.h"
#include "Core/Expression.h"
#include "Builder/MLBuilder.h"
#include "Builder/MLBuilderMLPSigmoid.h"
#include "Loss/LeastSquare.h"
#include "Loss/LeastSquareBinary.h"
#include "Type/BinaryFloat.h"

#include <random>

#include <future>

TEST(ComputationalGraphGenerator,SimpleTest)
{
    try
    {
        MLBuilderMLP builder;
        ComputationGraphGenerator<ExpressionGenerator<std::mt19937_64>> Graph(5);
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::mt19937_64 generator(seed);
        typedef std::mt19937_64 type_generator;
        Graph.GetGenerator().SetMaxDepth(1);
        std::function<double(type_generator&,size_t)> func=[&builder](type_generator& g,size_t depth)
        {
            std::uniform_real_distribution<double> distribution(-1.0,1.0);
            return distribution(g);
        };
        Graph.InsertGenerator(func,true);
        std::function<double(double,double)> funcAdd=[](double a,double b)
        {
            return a+b;
        };
        
        auto myFuncAdd=builder.Build(funcAdd);
        Graph.Insert(myFuncAdd);
        
        std::function<double(double,double)> funcLoss;
        auto loss=LeastSquareLoss(funcLoss);
        Graph.InsertLoss(loss);
        
        Graph.CreateGraphExecutor();
        auto stack=Graph.GenerateStack<double>(2,generator);
        auto error=Graph.run<double>(stack.first);
        std::cout<<"error "<<error<<std::endl;
    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}

TEST(ComputationalGraphGenerator,TestLearning)
{
    try
    {
        MLBuilderMLP builder;
        ComputationGraphGenerator<ExpressionGenerator<std::mt19937_64>> Graph(100);
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::mt19937_64 generator(seed);
        typedef std::mt19937_64 type_generator;
        Graph.GetGenerator().SetMaxDepth(2);
        std::function<double(type_generator&,size_t)> func=[&builder](type_generator& g,size_t depth)
        {
            std::uniform_real_distribution<double> distribution(-1.0,1.0);
            return distribution(g);
        };
        Graph.InsertGenerator(func,true);
        std::function<double(double,double)> funcAdd=[](double a,double b)
        {
            return a+b;
        };
        
        auto myFuncAdd=builder.Build(funcAdd);
        Graph.Insert(myFuncAdd);
        
        std::function<double(double,double)> funcSub=[](double a,double b)
        {
            return a-b;
        };
        
        auto myFuncSub=builder.Build(funcSub);
        Graph.Insert(myFuncSub);
        
        std::function<double(double,double)> funcLoss;
        auto loss=LeastSquareLoss(funcLoss);
        Graph.InsertLoss(loss);
        
        Graph.CreateGraphExecutor();
        
        auto param=Graph.GetParam();
        auto adamOption=torch::optim::AdamOptions(1e-2);
        torch::optim::Adam adam(param,adamOption);
        
        std::cout<<"size param outside"<<param.size()<<std::endl;
        
        std::for_each(param.begin(),param.end(),[](auto elem)
        {
            std::cout<<elem<<std::endl;
        });
        
        auto funcFill=[&Graph,&generator]()
        {
            return Graph.GenerateStack<double>(1,generator);
        };
        
        typedef decltype(std::async(std::launch::async,funcFill)) TypeFuture;
        
        std::queue<TypeFuture> queue;
        
        auto FillQueue=[&]()
        {
            for(size_t batch=0;batch<6;++batch)
            {
                auto input=std::async(std::launch::async,funcFill);
                queue.push(std::move(input));
            }
        };
        FillQueue();
        
        for(int i=0;i<100;++i)
        {
            adam.zero_grad();
            torch::autograd::Variable one=torch::autograd::make_variable(at::ones({1},at::kCUDA),false);
            auto stack=queue.front().get();
            queue.pop();
            auto inputnew=std::async(std::launch::async,funcFill);
            queue.push(std::move(inputnew));
            Graph.GetGenerator().SetMaxDepth(i%6);
            auto error=Graph.run<double>(stack.first);
            Backward(error,one);
            adam.step();
            std::cout<<"epoch "<<i<<std::endl;
            std::cout<<"error "<<error<<std::endl;
        }
    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}

struct HiddenDouble
{
    double value;
};

template <>
class ML<HiddenDouble>: public at::Tensor
{
    public:
    static size_t m_size;
    static size_t size()
    {
        return m_size;
    }
    ML()
    {
    }
    ML(at::Tensor embedded):at::Tensor(embedded)
    {
        
    }
    static constexpr bool IsFloatCompatible()
    {
        return false;
    }
    typedef HiddenDouble Type;
};

size_t ML<HiddenDouble>::m_size=1;

TEST(ComputationalGraphGenerator,TestLearning3)
{
    try
    {
        MLBuilderMLP builder;
        
        ComputationGraphGenerator<ExpressionGenerator<std::mt19937_64>> Graph(10);
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::mt19937_64 generator(seed);
        typedef std::mt19937_64 type_generator;
        Graph.GetGenerator().SetMaxDepth(2);
        
        std::function<BinaryFloat(type_generator&,size_t)> func=[&builder](type_generator& g,size_t depth)
        {
            std::uniform_real_distribution<double> distribution(-1.0,1.0);
            return BinaryFloat(distribution(g));
        };
        Graph.InsertGenerator(func,true);
        
        std::function<double(type_generator&,size_t)> func2=[&builder](type_generator& g,size_t depth)
        {
            std::uniform_real_distribution<double> distribution(-1.0,1.0);
            return distribution(g);
        };
        Graph.InsertGenerator(func2,true);
        
        std::function<HiddenDouble(double v)> funcToHidden=[](double v)
        {
            return HiddenDouble{v};
        };
        
        auto myFuncToHidden=builder.Build(funcToHidden);
        Graph.Insert(myFuncToHidden,true);
        
        std::function<double(HiddenDouble v)> funcHiddenToDouble=[](HiddenDouble v)
        {
            return v.value;
        };
        
        auto myFuncHiddenToDouble=builder.Build(funcHiddenToDouble);
        Graph.Insert(myFuncHiddenToDouble);
        
        std::function<HiddenDouble(BinaryFloat v)> funcToHidden2=[](BinaryFloat v)
        {
            return HiddenDouble{static_cast<double>(v)};
        };
        
        auto myFuncToHidden2=builder.Build(funcToHidden2);
        Graph.Insert(myFuncToHidden2,true);
        
        std::function<BinaryFloat(HiddenDouble v)> funcHiddenToBinary=[](HiddenDouble v)
        {
            return BinaryFloat(v.value);
        };
        
        auto myFuncHiddenToBinary=builder.Build(funcHiddenToBinary);
        Graph.Insert(myFuncHiddenToBinary);
       
        std::function<HiddenDouble(HiddenDouble,HiddenDouble)> funcAdd=[](HiddenDouble a,HiddenDouble b)
        {
            return HiddenDouble{a.value+b.value};
        };
        
        auto myFuncAdd=builder.Build(funcAdd);
        Graph.Insert(myFuncAdd);
        
        std::function<HiddenDouble(HiddenDouble,HiddenDouble)> funcSub=[](HiddenDouble a,HiddenDouble b)
        {
            return HiddenDouble{a.value-b.value};
        };
        
        auto myFuncSub=builder.Build(funcSub);
        Graph.Insert(myFuncSub);
        
        auto loss=LeastSquareLossBinary();
        Graph.InsertLoss(loss);
        
        std::function<double(double,double)> funcLoss;
        auto loss2=LeastSquareLoss(funcLoss);
        Graph.InsertLoss(loss2);
        
        Graph.CreateGraphExecutor();
        
        auto param=Graph.GetParam();
        auto adamOption=torch::optim::AdamOptions(1e-3);
        torch::optim::Adam adam(param,adamOption);
        
        auto funcFillDouble=[&Graph,&generator]()
        {
            return Graph.GenerateStack<double>(100,generator);
        };
        
        auto funcFillBinary=[&Graph,&generator]()
        {
            return Graph.GenerateStack<BinaryFloat>(100,generator);
        };
        
        typedef decltype(std::async(std::launch::async,funcFillDouble)) TypeFuture;
        
        std::queue<TypeFuture> queue;
        std::queue<TypeFuture> queue2;
        auto FillQueue=[&]()
        {
            for(size_t batch=0;batch<6;++batch)
            {
                auto input=std::async(std::launch::async,funcFillDouble);
                queue.push(std::move(input));
            }
            for(size_t batch=0;batch<6;++batch)
            {
                auto input=std::async(std::launch::async,funcFillBinary);
                queue2.push(std::move(input));
            }
        };
        FillQueue();
        
        auto funcLoop=[&]()
        {
            double valerror=200;
            for(int i=0;i<100;++i)
            {
                adam.zero_grad();
                torch::autograd::Variable one=torch::autograd::make_variable(at::ones({1},at::kCUDA),false);
                torch::jit::Stack stack;
                if(i%2==0)
                {
                    auto elem=queue.front().get();
                    queue.pop();
                    stack=elem.first;
                    auto inputnew=std::async(std::launch::async,funcFillDouble);
                    queue.push(std::move(inputnew));
                }
                else
                {
                    auto elem=queue2.front().get();
                    queue2.pop();
                    stack=elem.first;
                    auto inputnew=std::async(std::launch::async,funcFillBinary);
                    queue2.push(std::move(inputnew));
                }
                torch::autograd::Variable error;
                error=Graph.run<double>(stack);
                Backward(error,one);
                adam.step();
                valerror=error.item<double>();
                std::cout<<"epoch "<<i<<std::endl;
                std::cout<<"error "<<error<<std::endl;
            }
        };
        funcLoop();
    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}
