#include <gtest/gtest.h>
#include "Core/Done.h"

template <hasDone T>
void f(T)
{
};

TEST(DoneClass,NormalUsage)
{
    Done D;
    EXPECT_FALSE(D);
    EXPECT_FALSE(D.AlreadyDone());
    D.SetDone();
    EXPECT_TRUE(D);
    EXPECT_TRUE(D.AlreadyDone());
    f(D);
}

class testDone : public Done
{
    
};

TEST(DoneClass,inheritence)
{
    testDone D;
    EXPECT_FALSE(D);
    EXPECT_FALSE(D.AlreadyDone());
    D.SetDone();
    EXPECT_TRUE(D);
    EXPECT_TRUE(D.AlreadyDone());
    f(D);
};
