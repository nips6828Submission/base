#include "Core/StartUp.h"
StartUp start;
#include <gtest/gtest.h>
#include "torch/csrc/autograd/variable.h"
#include "torch/csrc/jit/symbolic_variable.h"
#include "torch/csrc/jit/autodiff.h"
#include "torch/csrc/jit/graph_executor.h"
#include "torch/csrc/autograd/function.h"
#include "torch/csrc/jit/passes/to_batch.h"
#include "torch/csrc/jit/batched/BatchTensor.h"
#include "torch/csrc/jit/custom_operator.h"
#include "Core/ExpressionGenerator.h"

#include "Core/Backward.h"
#include <functional>
#include "Core/FunctionOp.h"

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <fstream>
#include <boost/serialization/vector.hpp>
#include "Serialization/Tensor.h"

#include "Core/Symbolic_Variable.h"
#include "Core/Expression.h"
#include "Builder/MLBuilder.h"
#include "Builder/MLBuilderMLP.h"
#include "Core/ExpressionsToGraph.h"

#include <boost/graph/graphml.hpp>

#include <random>

TEST(ExpressionGenerator,SimpleTest)
{
    try
    {
        MLBuilderMLP builder;
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::cout<<"seed "<<seed<<std::endl;
        std::mt19937_64 generator(seed);
        typedef std::mt19937_64 type_generator;
        ExpressionGenerator<std::mt19937_64> Generator;
                
        std::function<double(type_generator&,size_t)> func=[&builder](type_generator& g,size_t depth)
        {
            std::uniform_real_distribution<double> distribution(-1.0,1.0);
            return distribution(g);
        };
        Generator.InsertGenerator(func,true);
        std::function<double(double,double)> funcAdd=[](double a,double b)
        {
            return a+b;
        };
        auto myFuncAdd=builder.Build(funcAdd);
        Generator.Insert(myFuncAdd);
        
        auto ffunc=Generator.GetRandom<double>(generator);
        auto expressions=ffunc(generator,0);
        std::for_each(expressions.begin(),expressions.end(),[](auto elem)
        {
            std::cout<<elem<<std::endl;
        });
    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}

TEST(ExpressionGenerator,TestGeneratorFunction)
{
    try
    {
        MLBuilderMLP builder;
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::cout<<"seed "<<seed<<std::endl;
        std::mt19937_64 generator(seed);
        typedef std::mt19937_64 type_generator;
        ExpressionGenerator<std::mt19937_64> Generator;
                
        std::function<double(type_generator&,size_t)> func=[&builder](type_generator& g,size_t depth)
        {
            std::uniform_real_distribution<double> distribution(-1.0,1.0);
            return distribution(g);
        };
        Generator.InsertGenerator(func,true);
        std::function<double(double,double)> funcAdd=[](double a,double b)
        {
            return a+b;
        };
        std::function<std::tuple<double,double>(std::tuple<double,double> tuple,type_generator g,Expression& expression)> funGenAdd=[](std::tuple<double,double> tuple,type_generator g,Expression& expression)
        {
            std::uniform_real_distribution<double> distribution(-20.0,20.0);
            double val=distribution(g);
            std::get<0>(tuple)=val;
            expression.m_inputs[0].Set(val);
            return tuple;
        };
        auto myFuncAdd=builder.Build(funcAdd);
        Generator.Insert(myFuncAdd,funGenAdd);
        
        auto ffunc=Generator.GetRandom<double>(generator);
        auto expressions=ffunc(generator,0);
        std::for_each(expressions.begin(),expressions.end(),[](auto elem)
        {
            std::cout<<elem<<std::endl;
        });
    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}

#include <stdexcept>

TEST(ExpressionGenerator,TestExceptionRetry)
{
    try
    {
        MLBuilderMLP builder;
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::cout<<"seed "<<seed<<std::endl;
        std::mt19937_64 generator(seed);
        typedef std::mt19937_64 type_generator;
        ExpressionGenerator<std::mt19937_64> Generator;
        Generator.SetMaxDepth(2);
                
        std::function<double(type_generator&,size_t)> func=[&builder](type_generator& g,size_t depth)
        {
            std::uniform_real_distribution<double> distribution(-1.0,1.0);
            return distribution(g);
        };
        Generator.InsertGenerator(func,true);
        std::function<double(double,double)> funcAdd=[](double a,double b)
        {
            throw std::logic_error("Error");
            return a+b;
        };
        auto myFuncAdd=builder.Build(funcAdd);
        Generator.Insert(myFuncAdd);
        
        auto ffunc=Generator.GetRandom<double>(generator);
        auto expressions=ffunc(generator,0);
        std::for_each(expressions.begin(),expressions.end(),[](auto elem)
        {
            std::cout<<elem<<std::endl;
        });
    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}

TEST(ExpressionGenerator,TestToGraph)
{
    try
    {
        MLBuilderMLP builder;
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::cout<<"seed "<<seed<<std::endl;
        std::mt19937_64 generator(seed);
        typedef std::mt19937_64 type_generator;
        ExpressionGenerator<std::mt19937_64> Generator;
        Generator.SetMaxDepth(2);
                
        std::function<double(type_generator&,size_t)> func=[&builder](type_generator& g,size_t depth)
        {
            std::uniform_real_distribution<double> distribution(-1.0,1.0);
            return distribution(g);
        };
        Generator.InsertGenerator(func,true);
        std::function<double(double,double)> funcAdd=[](double a,double b)
        {
            return a+b;
        };
        auto myFuncAdd=builder.Build(funcAdd);
        Generator.Insert(myFuncAdd);
        
        auto ffunc=Generator.GetRandom<double>(generator);
        auto expressions=ffunc(generator,0);
        std::for_each(expressions.begin(),expressions.end(),[](auto elem)
        {
            std::cout<<elem<<std::endl;
        });
        auto graph=ExpressionsToGraph(expressions);
        boost::dynamic_properties dp;
        dp.property("Function", boost::get(&ExpressionsVertexProperties::functionId, graph));
        boost::write_graphml(std::cout, graph, dp, false);
    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}
