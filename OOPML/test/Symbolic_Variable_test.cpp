#include "Core/StartUp.h"
StartUp start;
#include <gtest/gtest.h>
#include "torch/csrc/autograd/variable.h"
#include "torch/csrc/jit/symbolic_variable.h"
#include "torch/csrc/jit/autodiff.h"
#include "torch/csrc/jit/graph_executor.h"
#include "torch/csrc/autograd/function.h"
#include "torch/csrc/jit/passes/to_batch.h"
#include "torch/csrc/jit/batched/BatchTensor.h"
#include "torch/csrc/jit/custom_operator.h"

#include "Core/Backward.h"
#include <functional>
#include "Core/FunctionOp.h"

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <fstream>
#include <boost/serialization/vector.hpp>
#include "Serialization/Tensor.h"

#include "Core/Symbolic_Variable.h"

TEST(Symbolic_Variable,testUsage)
{
    try
    {
        Symbolic_Variable Var=Symbolic_Variable::NewVariable();
        std::cout<<"Variable "<<Var<<std::endl;
        Symbolic_Variable Var2=Var;
        Symbolic_Variable Var3=Symbolic_Variable::NewVariable();
        EXPECT_EQ(Var,Var2);
        EXPECT_NE(Var,Var3);
    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}

