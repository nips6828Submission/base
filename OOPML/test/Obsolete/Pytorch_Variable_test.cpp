//#include "Core/StartUp.h"
//StartUp start;
#include <gtest/gtest.h>
#include <torch/csrc/autograd/variable.h>
#include <torch/csrc/autograd/python_variable.h>
#include <torch/csrc/autograd/function.h>
#include "Core/CompGraph.h"

//PyMODINIT_FUNC PyInit__C();

/*
TEST(PytorchVariable,main)
{
        std::function<torch::autograd::Variable(torch::autograd::Variable)> func=[](torch::autograd::Variable var)
        {
            return var;
        };
        at::Tensor d = at::CUDA(at::kFloat).ones({1, 1})*2;
        torch::autograd::Variable var=torch::autograd::make_variable(d,true);
        //torch::autograd::Variable var2=2*var;
        //torch::autograd::Variable var3=2*var2;
        //torch::autograd::Variable var4=3*var2;
        //torch::autograd::Variable var5=var3+var4;
        //torch::autograd::Variable var6=10*var5;
        CompGraph graph(func(var));
        at::Tensor d2 = at::CUDA(at::kFloat).ones({1, 1})*10;
        torch::autograd::Variable var2=torch::autograd::make_variable(d2,true);
        
        at::Tensor d3 = at::CUDA(at::kFloat).ones({1, 1})*200;
        torch::autograd::Variable var3=torch::autograd::make_variable(d3,true);
        auto edge=var2.gradient_edge();
        std::cout<<"grad bef"<<std::endl;
        std::cout<<dynamic_cast<torch::autograd::AccumulateGrad*>(edge.function.get())->variable<<std::endl;
        auto grad=graph.GetGradient(edge);
        std::vector<torch::autograd::Variable> var_in;
        var_in.push_back(var3);
        grad(var_in);
        std::cout<<var.grad()<<std::endl;
}*/

/*
TEST(PytorchVariable,main2)
{
        std::function<torch::autograd::Variable(torch::autograd::Variable)> func=[](torch::autograd::Variable var)
        {
            return (var+var*var+70)*var;
        };
        at::Tensor d = at::CUDA(at::kFloat).ones({1, 1})*2;
        torch::autograd::Variable var=torch::autograd::make_variable(d,true);
        //torch::autograd::Variable var2=2*var;
        //torch::autograd::Variable var3=2*var2;
        //torch::autograd::Variable var4=3*var2;
        //torch::autograd::Variable var5=var3+var4;
        //torch::autograd::Variable var6=10*var5;
        CompGraph graph(func(var));
        at::Tensor d2 = at::CUDA(at::kFloat).ones({1, 1})*10;
        torch::autograd::Variable var2=torch::autograd::make_variable(d2,true);
        
        auto res=func(var2);
        auto edge=res.gradient_edge();
        auto grad=graph.GetGradient(edge);
        
        at::Tensor d3 = at::CUDA(at::kFloat).ones({1, 1})*1;
        torch::autograd::Variable var3=torch::autograd::make_variable(d3,true);

        std::vector<torch::autograd::Variable> var_in;
        var_in.push_back(var3);
        grad(var_in);
        std::cout<<var.grad()<<std::endl;
        //graph.Print();
}
*/

TEST(PytorchVariable,main3)
{
        std::function<torch::autograd::Variable(torch::autograd::Variable,torch::autograd::Variable,torch::autograd::Variable)> func=[](torch::autograd::Variable w,torch::autograd::Variable x,torch::autograd::Variable y)
        {
            torch::autograd::Variable res=w.mv(x)-y;
            return res*res;
        };
        at::Tensor dw = at::ones({1,1000},at::kCUDA)*2;
        torch::autograd::Variable w=torch::autograd::make_variable(dw,true);
        at::Tensor dx = at::ones({1000},at::kCUDA)*2;
        torch::autograd::Variable x=torch::autograd::make_variable(dx,true);
        at::Tensor dy = at::ones({1},at::kCUDA)*2;
        torch::autograd::Variable y=torch::autograd::make_variable(dy,true);
        CompGraph graph(func(w,x,y));
        at::Tensor dgrad = at::ones({1},at::kCUDA)*1;
        torch::autograd::Variable vargrad=torch::autograd::make_variable(dgrad,true);
        std::vector<torch::autograd::Variable> var_in;
        var_in.push_back(vargrad);
        std::chrono::steady_clock clock;
        auto start_time = clock.now();
        typedef std::chrono::duration<double> duration_t;
        for(int i=0;i<10000;++i)
        {
            at::Tensor dw2 = at::ones({1, 1000},at::kCUDA)*i;
            torch::autograd::Variable w2=torch::autograd::make_variable(dw2,true);
            at::Tensor dx2 = at::ones({1000},at::kCUDA)*2;
            torch::autograd::Variable x2=torch::autograd::make_variable(dx2,true);
            at::Tensor dy2 = at::ones({1},at::kCUDA)*2;
            torch::autograd::Variable y2=torch::autograd::make_variable(dy2,true);
            auto res=func(w2,x2,y2);
            auto edge=res.gradient_edge();
            auto grad=graph.GetGradient(edge);
            grad(var_in);
        }
        duration_t diff_from_start=clock.now()-start_time;
        std::cout<<"time "<<diff_from_start.count()/10000<<std::endl;
//        std::cout<<w.grad()<<std::endl;
        //graph.Print();
}
