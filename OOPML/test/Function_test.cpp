#include <gtest/gtest.h>
#include <iostream>
#include <string>
#include <functional>
#include <any>
#include <vector>
#include <typeindex>
#include <random>
#include <tuple>
#include "Core/Function.h"
//#include "Core/FunctionList.h"
#include <chrono>
#include "Builder/MLBuilder.h"
#include "Builder/MLBuilderMLP.h"

struct Foo 
{
    Foo(){};
    void f1()
    {
        std::cout<<"function void(void)"<<std::endl;
    }
    void f2(int i)
    {
        std::cout<<"function void(int)"<<std::endl;
    }
    int f3(int i)
    {
        std::cout<<"function int(int)"<<std::endl;
        return i;
    }
};

TEST(function, Constructor)
{
    std::function<void(Foo)> f1=&Foo::f1;
    std::function<void(Foo,int)> f2=&Foo::f2;
    std::function<int(Foo,int)> f3=&Foo::f3;
    Foo foo;
    f1(foo);
    int i=0;
    f2(foo,i);
    f3(foo,i);
}

TEST(function, any)
{
    std::function<void(Foo)> f1=&Foo::f1;
    std::function<void(Foo,int)> f2=&Foo::f2;
    std::function<int(Foo,int)> f3=&Foo::f3;
    std::vector<std::any> vector;
    vector.push_back(std::any(f1));
    vector.push_back(std::any(f2));
    vector.push_back(std::any(f3));
    Foo foo;
    int i=0;
    if(vector[0].type()==typeid(std::function<void(Foo)>))
    {
        std::any_cast<std::function<void(Foo)>> (vector[0])(foo);
    }
    if(vector[1].type()==typeid(std::function<void(Foo,int)>))
    {
        std::any_cast< std::function<void(Foo,int)>> (vector[1])(foo,i);
    }
}


TEST(function,list_Function)
{
    std::multimap<std::type_index,std::function<std::any(int)>> mymap;
    std::function<std::any(int)> f=[](int seed)
    {
        return 3.0;
    };
    mymap.insert({std::type_index(typeid(double)),f});
}

TEST(function,argument)
{
    typedef std::function<double(double,long)> type_func;
    std::cout<<SizeArgument<type_func>::size<<std::endl;
    std::cout<<typeid(TypeArgument<type_func,1>::type).name()<<std::endl;
}

/*
TEST(function,function_List)
{
    typedef std::default_random_engine type_generator;
    MLBuilder build;
    FunctionList<type_generator,MLBuilder> list(build);
    int seed=std::chrono::system_clock::now().time_since_epoch().count();
    type_generator generator(seed);
    typedef std::function<double(double,long)> type_func;
    type_func f=[](double v,long i)
    {
        std::cout<<"f "<<v<<" "<<i<<std::endl;
        return v+i;
    };
    list.Insert(f);
    
    std::function<double(type_generator& generator,size_t depth)>f2=[](type_generator& generator,size_t depth)
    {
        std::cout<<"f2"<<std::endl;
        std::uniform_real_distribution<double> distribution (0.0,100.0);
        auto res=distribution(generator);
        std::cout<<"res "<<res<<std::endl;
        return res;
    };
    list.InsertDirect(f2);
    std::function<long(type_generator& generator,size_t depth)>f3=[](type_generator& generator,size_t depth)
    {
        std::cout<<"f3 "<<std::endl;
        std::uniform_int_distribution<long> distribution(1,10);
        auto res=distribution(generator);
        std::cout<<"res "<<res<<std::endl;
        return res;
    };
    list.InsertDirect(f3);
    std::cout<<"starting"<<std::endl;
    auto val=list.GetRandom(std::type_index(typeid(double)),generator);
    //std::cout<<std::any_cast<double>(val(generator,0).first)<<std::endl;
}
*/

