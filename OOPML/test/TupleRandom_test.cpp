#include <gtest/gtest.h>
#include <iostream>
#include <tuple>
#include <random>
#include <any>
#include <string>


TEST(TupleRandom, example)
{
    //std::any a(std::string("dede"));
    std::any a(20);
    std::cout<<a.type().name()<<std::endl;
    if(a.type()==typeid(std::string))
    {
        std::cout<<"ok string"<<std::endl;
    }
    else
    {
        std::cout<<"something else"<<std::endl;
    }
}
