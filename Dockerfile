from registry.gitlab.com/nips6828submission/cmake as cmake

from registry.gitlab.com/nips6828submission/pytorch:latest
MAINTAINER nips6828@gmail.com
env NVIDIA_VISIBLE_DEVICES=void
run apt-get update -y && apt-get install -y libboost-all-dev qttools5-dev-tools qt5-default graphviz vim libcereal-dev
COPY --from=cmake / /usr/local
COPY OOPML /workspace/OOPML/ 
ENV PyTorch_Lib_Path="/usr/local/lib/python3.6/dist-packages/torch"
ENV PyTorchC_Lib_Path="/usr/local/lib/python3.6/dist-packages/torch/lib"
ENV PyTorch_Inc_Path="/usr/local/lib/python3.6/dist-packages/torch/include;/usr/local/lib/python3.6/dist-packages/torch/include/TH;/usr/local/lib/python3.6/dist-packages/torch/include/torch/csrc;/usr/local/lib/python3.6/dist-packages/torch/include/torch/csrc/api/include;/workspace/pytorch/torch/include"
ENV Caffe_Lib_Path="/usr/local/lib/python3.6/dist-packages/torch/lib"
ENV C10_Lib_Path="/usr/local/lib/python3.6/dist-packages/torch/lib"
run apt-get install -y libcereal-dev
workdir /usr/local/lib/python3.6/dist-packages/torch/jit
COPY batchop.py .
workdir /workspace/OOPML
run mkdir -p /Data
env NVIDIA_VISIBLE_DEVICES=all
